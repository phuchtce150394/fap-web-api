using System.ComponentModel.DataAnnotations;
using FapWebApi.Models;

namespace FapWebApi.Identity {

    public class RegisterModel {

        [Required(ErrorMessage = "UserName can't be blank.")]
        [MinLength(4)]  
        public string UserName { get; set; }
  
        [EmailAddress]  
        [Required(ErrorMessage = "Email can't be blank.")]
        public string Email { get; set; }
  
        [Required(ErrorMessage = "Password can't be blank")]
        [MinLength(8)]
        public string Password { get; set; }

    }

}