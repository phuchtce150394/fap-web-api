using System.Threading.Tasks;
using FapWebApi.Models;
using Microsoft.AspNetCore.Identity;

namespace FapWebApi.Identity
{  
    /// Author: phuchtce150394
    /// <summary>
    /// User roles management
    /// </summary>
    public static class UserRoles  
    {  
        public const string Admin   = "Admin";  
        public const string Teacher = "Teacher";
        public const string Parent = "Parent";
        public const string Student = "Student";

        public static async Task CreateRolesAsync(UserManager<Account> userManager, RoleManager<IdentityRole> roleManager)
        {
            // Create all Role for System
            if (!await roleManager.RoleExistsAsync(UserRoles.Admin))  
                await roleManager.CreateAsync(new IdentityRole(UserRoles.Admin));

            if (!await roleManager.RoleExistsAsync(UserRoles.Parent))  
                await roleManager.CreateAsync(new IdentityRole(UserRoles.Parent));

            if (!await roleManager.RoleExistsAsync(UserRoles.Teacher))  
                await roleManager.CreateAsync(new IdentityRole(UserRoles.Teacher));

            if (!await roleManager.RoleExistsAsync(UserRoles.Student))  
                await roleManager.CreateAsync(new IdentityRole(UserRoles.Student));
        }
        
    }  
}  