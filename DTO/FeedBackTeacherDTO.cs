using System;
using FapWebApi.Models;

namespace FapWebApi.DTO
{
    public class FeedbackTeacherDTO
    {
        public int Id { get; }
        public string StudentRollNumber { get; set; }
        public string TeacherRollNumber { get; set; }
        public string Content { get; set; }
        public DateTime Created { get; }

        public FeedbackTeacherDTO(){}

        public FeedbackTeacherDTO(FeedbackTeacher obj){
            Id=obj.Id;
            StudentRollNumber=obj.StudentRollNumber;
            TeacherRollNumber=obj.TeacherRollNumber;
            Content=obj.Content;
            Created=obj.Created;
        }

    }
}