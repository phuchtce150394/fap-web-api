using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FapWebApi.DTO
{
    public class PostMultipleScheduleDTO
    {
        [Required(ErrorMessage = "Missing ClassName!")]
        public string ClassName { get; set; }

        [Required(ErrorMessage = "Missing SubjectCode!")]
        public string SubjectCode { get; set; }

        [Required(ErrorMessage = "Missing TeacherCode!")]
        public string TeacherCode { get; set; }

        public string RoomCode { get; set; }

        [Required(ErrorMessage = "Missing NumberOfSlots!")]
        public int NumberOfSlots { get; set; }

        [Required(ErrorMessage = "Missing SemesterCode!")]
        public string SemesterCode { get; set; }

        [Required(ErrorMessage = "Missing StartDate!")]
        public DateTime StartDate { get; set; }
        public List<int[]> CycleDetails { get; set; }

        public string Note { get; set; }

    }
}