

using System;
using System.ComponentModel.DataAnnotations;
using FapWebApi.Models;

namespace FapWebApi.DTO
{
    public class AdminProfileDTO
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public string ImageURL { get; set; } = "/media/default.png";
        
        public string Name{ get; set; }

        public bool? Gender {get; set;}

        public string RollNumber{ get; set; }
        
        public DateTime? Birthday { get; set; }

        public AdminProfileDTO() {}

        public AdminProfileDTO(AdminProfile adminProfile) {
            Id=adminProfile.Id;
            Name=adminProfile.Name;
            ImageURL=adminProfile.ImageURL;
            Gender=adminProfile.Gender;
            Birthday=adminProfile.Birthday;
            RollNumber=adminProfile.RollNumber;
            UserId=adminProfile.AccountId;
        }

    }
}