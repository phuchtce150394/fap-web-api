using System;
using FapWebApi.Models;

namespace FapWebApi.DTO
{
    public class SemesterDTO {

        public int Id { get; }
        
        public string Code { get; set; }

        public string Name { get; set; }

        public DateTime StartDate { get; set; }
        
        public DateTime EndDate { get; set; }

        public SemesterDTO(){}

        public SemesterDTO(Semester obj) {
            Id=obj.Id;
            Code=obj.Code;
            Name=obj.Name;
            StartDate=obj.StartDate;
            EndDate=obj.EndDate;
        }

    }
}