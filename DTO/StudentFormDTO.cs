

using FapWebApi.Models;

namespace FapWebApi.DTO
{
    public class StudentFormDTO 
    {

        public int Id { get; }

        public string StudentRollNumber { get; set; }

        public string ApplicationType { get; set; }

        public string Purpose { get; set; }

        public string FileURL { get; }

        public bool isDone { get; }

        public StudentFormDTO(){}
        public StudentFormDTO(StudentForm form)
        {
            Id=form.Id;
            StudentRollNumber=form.StudentRollNumber;
            ApplicationType=form.ApplicationType;
            Purpose=form.ApplicationType;
            FileURL=form.FileURL;
            isDone=form.isDone;
        }

    }
}