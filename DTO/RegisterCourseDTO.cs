using FapWebApi.Models;

namespace FapWebApi.DTO
{
    public class RegisterCourseDTO 
    {
        public int Id { get; }
        public string StudentRollNumber { get; set; }

        public string SubjectCode { get; set; }

        public string SemesterCode { get; set; }

        public string Status { get; set; }

        public bool isPayment { get; }

        public RegisterCourseDTO(){}

        public RegisterCourseDTO(RegisterCourse course) {
            Id=course.Id;
            StudentRollNumber=course.StudentRollNumber;
            SubjectCode=course.SubjectCode;
            SemesterCode=course.SemesterCode;
            Status=course.Status;
            isPayment=course.isPayment;
        }

    }
}