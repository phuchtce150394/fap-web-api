using System.Collections.Generic;
using System.Linq;
using FapWebApi.Models;

namespace FapWebApi.DTO
{
    
    public class MajorDTO {

        public int Id { get; }

        public string Code { get; set; }

        public string Name { get; set; }

        public int MajorGroupId { get; set; }

        public MajorGroup MajorGroup { get; }

        // MappingAllSubjects
        public ICollection<SubjectDTO> Subjects { get; }

        public MajorDTO(){}

        public MajorDTO(Major major) {
            Id=major.Id;
            Code=major.Code;
            Name=major.Name;
            MajorGroupId=major.MajorGroupId;
            MajorGroup=major.MajorGroup;
            Subjects=major.Subjects?.Select(subject => new SubjectDTO(subject)).ToList<SubjectDTO>();
        }

    }

}