using FapWebApi.Models;

namespace FapWebApi.DTO
{
    public class RoomDTO {

        public int Id { get; }

        public string RoomCode { get; set; }

        public bool IsActive { get; set; } = true;

        public string Note { get; set; }

        public RoomDTO(){}

        public RoomDTO(Room room) {
            Id=room.Id;
            RoomCode=room.RoomCode;
            IsActive=room.IsActive;
            Note=room.Note;
        }

    }
}