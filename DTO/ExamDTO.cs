using System;
using FapWebApi.Models;

namespace FapWebApi.DTO
{
    public class ExamDTO 
    {
        public int Id { get; }

        public int RegisterCourseId { get; set; }

        public RegisterCourseDTO Course { get; }

        public DateTime Date { get; set; }
        
        public string RoomCode { get; set; }

        public string Time { get; set; }

        public string ExamForm { get; set; }

        public bool isDone { get; set; } = false;

        public ExamDTO(){}

        public ExamDTO(Exam obj) 
        {
            Id=obj.Id;
            RegisterCourseId=obj.RegisterCourseId;
            Course=obj.Course != null ? new RegisterCourseDTO(obj.Course):null;
            Date=obj.Date.Date;
            RoomCode=obj.RoomCode;
            Time=obj.Time;
            ExamForm=obj.ExamForm;
            isDone=obj.isDone;
        }

    }
}