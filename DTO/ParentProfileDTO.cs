using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FapWebApi.Models;

namespace FapWebApi.DTO
{
    public class ParentProfileDTO
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int StudentProfileId { get; set; }

        public ParentProfileDTO() {}

        public ParentProfileDTO(ParentProfile profile) {
            Id = profile.Id;
            UserId = profile.AccountId;
            Name = profile.Name;
            Address = profile.Address;
            StudentProfileId = profile.StudentProfileId;
        }
        
    }
}