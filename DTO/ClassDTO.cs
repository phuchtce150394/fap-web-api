using FapWebApi.Models;

namespace FapWebApi.DTO
{
    public class ClassDTO {

        public int Id { get; }

        public string Name { get; set; }

        public int Status { get; set; } = 1;

        public int SemesterId { get; set; }

        // public Semester Semester { get; set; }

        public int NumberOfStudents { get; }

        public ClassDTO(){}

        public ClassDTO(Class item) {

            if(item.Id != 0) Id=item.Id;
            if(item.Name != null) Name=item.Name;
            Status=item.Status;
            if(item.SemesterId != 0) SemesterId=item.SemesterId;
            if(item.StudentProfiles != null) NumberOfStudents=item.StudentProfiles.Count;

        }

    }
}