

using System;
using System.ComponentModel.DataAnnotations;
using FapWebApi.Models;

namespace FapWebApi.DTO
{
    public class TeacherProfileDTO
    {

        public int Id { get; set; }

        public string UserId { get; set; }
        
        public string Name { get; set; }

        public string ImageURL { get; set; } = "/media/default.png";

        public bool? Gender { get; set; } = true;

        public string RollNumber { get; set; }

        public DateTime? Birthday { get; set; }

        public string PhoneNumber { get; set; }

        public TeacherProfileDTO() {}

        public TeacherProfileDTO(TeacherProfile profile)
        {
            Id=profile.Id;
            Name=profile.Name;
            ImageURL=profile.ImageURL;
            RollNumber=profile.RollNumber;
            Gender=profile.Gender;
            Birthday=profile.Birthday;
            PhoneNumber=profile.PhoneNumber;
            UserId=profile.AccountId;
        }
    }
}