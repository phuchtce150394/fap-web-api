using System;
using FapWebApi.Models;


namespace FapWebApi.DTO
{
    public class NotificationDTO {

        public int Id { get; }

        public string Title { get; set; }
        
        public string Content { get; set; }
        
        public DateTime DateTimeOfPublic { get; }

        public NotificationDTO(){}

        public NotificationDTO(Notification obj) {

            Id=obj.Id;
            Title=obj.Title;
            Content=obj.Content;
            DateTimeOfPublic=obj.DateTimeOfPublic;

        }

    }
}