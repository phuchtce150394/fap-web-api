

using FapWebApi.Models;

namespace FapWebApi.DTO
{
    public class StudentFeeDTO
    {

        public int Id { get; }
        public string StudentRollNumber { get; set; }
        public string StudentName { get; }
        public string SemesterCode { get; set; }
        public int TotalFee { get; }
        public int PaidFee { get; }
        public bool isComplete { get; }
        public string Note { get; set; }

        public StudentFeeDTO(){}
        public StudentFeeDTO(StudentFee obj) {
            Id=obj.Id;
            StudentRollNumber=obj.StudentRollNumber;
            StudentName=obj.StudentName;
            SemesterCode=obj.SemesterCode;
            TotalFee=obj.TotalFee;
            PaidFee=obj.PaidFee;
            isComplete=obj.isComplete;
            Note=obj.Note;
        }

    }
}