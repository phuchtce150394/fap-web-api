

using FapWebApi.Models;

namespace FapWebApi.DTO
{
    public class SubjectDTO
    {

        public int Id { get; }

        public int Type { get; set; } = 1;
        public string Code { get; set; }
        public string Name { get; set; }
        public int TermNo { get; set; }
        public int Credit { get; set; }
        public int Fee { get; set; }

        public string MajorCode { get; set; }

        public SubjectDTO() {}

        public SubjectDTO(Subject subject) {
            
            Id=subject.Id;
            Type=subject.Type;
            Code=subject.Code;
            Name=subject.Name;
            TermNo=subject.TermNo;
            Credit=subject.Credit;
            Fee=subject.Fee;
            MajorCode=subject.Major?.Code;
            
        }

    }
}