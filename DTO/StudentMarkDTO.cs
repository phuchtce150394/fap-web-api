

using FapWebApi.Models;

namespace FapWebApi.DTO
{
    
    public class StudentMarkDTO {

        public int Id { get; }

        public int RegisterCourseId { get; set; }
        public RegisterCourseDTO Course { get; }

        public string GradeCategory { get; set; }
        public string GradeItem { get; set; }
        public double Weight { get; set; }

        public double Value { get; set; }
        public string Comment { get; set; }

        public StudentMarkDTO(){}

        public StudentMarkDTO(StudentMark mark)
        {
            Id=mark.Id;
            RegisterCourseId=mark.RegisterCourseId;
            Course= mark.Course != null ? new RegisterCourseDTO(mark.Course): null;
            GradeCategory=mark.GradeCategory;
            GradeItem=mark.GradeItem;
            Weight=mark.Weight;
            Value=mark.Value;
            Comment=mark.Comment;
        }

    }

}