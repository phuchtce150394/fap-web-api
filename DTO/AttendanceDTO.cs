using System;
using FapWebApi.Models;

namespace FapWebApi.DTO
{

    public class AttendanceDTO
    {

        public int Id { get; }

        public string SemesterCode { get; }

        public string StudentCode { get; }

        public string SubjectCode { get; }

        public string Date { get; }

        public int? SlotNo { get; }

        public string RoomCode { get; }

        public string Lecturer { get; }

        public string ClassName { get; }

        public bool isPresent { get; }

        public string Comment { get; set; }

        public AttendanceDTO(Attendance obj)
        {
            Id=obj.Id;
            SemesterCode=obj.Course?.SemesterCode;
            StudentCode=obj.Course?.StudentRollNumber;
            SubjectCode=obj.Course?.SubjectCode;

            Date=obj.Schedule?.Date.ToShortDateString();
            SlotNo=obj.Schedule?.SlotNo;
            RoomCode=obj.Schedule?.RoomCode;
            Lecturer=obj.Schedule?.TeacherCode;
            ClassName=obj.Schedule?.ClassName;

            isPresent=obj.isPresent;
            Comment=obj.Comment;

        }

    }

}