

using System;
using System.ComponentModel.DataAnnotations;
using FapWebApi.Models;

namespace FapWebApi.DTO
{
    public class StudentProfileDTO
    {
        public string UserId { get; set; }

        public int Id { get; set; }
        
        public string Name { get; set; }

        public string ImageURL { get; set; } = "/media/default.png";

        public DateTime? Birthday { get; set; }

        public bool? Gender { get; set; }

        public string IdCard { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public string RollNumber { get; set; }
        
        public int? MajorId { get; set; }

        public StudentProfileDTO () {}

        public StudentProfileDTO(StudentProfile studentProfile) {
            
            Id = studentProfile.Id;
            Name = studentProfile.Name;
            ImageURL = studentProfile.ImageURL;
            UserId = studentProfile.AccountId;
            Gender = studentProfile.Gender;
            Address = studentProfile.Address;
            PhoneNumber = studentProfile.PhoneNumber;
            Birthday = studentProfile.Birthday;
            IdCard = studentProfile.IdCard;
            RollNumber = studentProfile.RollNumber;
            MajorId = studentProfile.MajorId;
        }

    }
}