using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FapWebApi.Models;

namespace FapWebApi.DTO
{   
    public class ScheduleDTO
    {
     
        public int Id { get; }
        public string ClassName { get; set; }

        public string SubjectCode { get; set; }
        public string TeacherCode { get; set; }
        public string RoomCode { get; set; }
        public DateTime Date { get; set; }
        public int SlotNo { get; set; }

        public ScheduleDTO(){}

        public ScheduleDTO(Schedule schedule) {
            Id=schedule.Id;
            ClassName=schedule.ClassName;
            SubjectCode=schedule.SubjectCode;
            TeacherCode=schedule.TeacherCode;
            RoomCode=schedule.RoomCode;
            Date=schedule.Date;
            SlotNo=schedule.SlotNo;
        }

    }



}