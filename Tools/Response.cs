namespace FapWebApi.Tools 
{  
    public class Response
    {  
        public string Status { get; set; }  
        public string Message { get; set; }
    }  
}