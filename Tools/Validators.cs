using System.Collections.Generic;

namespace FapWebApi.Tools
{
    
    public class Validators {

        public static string StringChoices(IList<string> requireList, string field) {
            return requireList.Contains(field) ? field : null;
        }

    }

}