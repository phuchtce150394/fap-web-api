using System;
using FapWebApi.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FapWebApi.Data
{
    public class ApplicationDbContext : IdentityDbContext<Account>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            base.OnModelCreating(builder);

            builder.Entity<RegisterCourse>()
                .HasIndex(rCourse => new {rCourse.StudentRollNumber, rCourse.SubjectCode, rCourse.SemesterCode})
                .IsUnique();
            builder.Entity<Schedule>()
                .HasIndex(s => new {s.ClassName, s.Date, s.SlotNo})
                .IsUnique();
            builder.Entity<Schedule>()
                .HasIndex(s => new {s.TeacherCode, s.Date, s.SlotNo})
                .IsUnique();

            builder.Entity<StudentMark>()
                .HasIndex(m => new {m.RegisterCourseId, m.GradeCategory, m.GradeItem})
                .IsUnique();

            builder.Entity<Attendance>()
                .HasIndex(attendance => new {attendance.RegisterCourseId, attendance.ScheduleId})
                .IsUnique();
                
            // Bỏ tiền tố AspNet của các bảng: mặc định các bảng trong IdentityDbContext có
            // tên với tiền tố AspNet như: AspNetUserRoles, AspNetUser ...
            // Đoạn mã sau chạy khi khởi tạo DbContext, tạo database sẽ loại bỏ tiền tố đó
            foreach (var entityType in builder.Model.GetEntityTypes())
            {
                var tableName = entityType.GetTableName();
                if (tableName.StartsWith("AspNet"))
                {
                    entityType.SetTableName(tableName.Substring(6));
                }
            }

        }
        
        public DbSet<Class> Classes { get; set; }

        public DbSet<Major> Majors { get; set; }

        public DbSet<MajorGroup> MajorGroups { get; set; }

        public DbSet<Notification> Notifications { get; set; }

        public DbSet<Semester> Semesters { get; set; }

        public DbSet<Subject> Subjects { get; set; }

        public DbSet<StudentProfile> StudentProfiles { get; set; }
        
        public DbSet<ParentProfile> ParentProfiles { get; set; }

        public DbSet<TeacherProfile> TeacherProfiles { get; set; }

        public DbSet<AdminProfile> AdminProfiles { get; set; }

        public DbSet<Room> Rooms { get; set; }

        public DbSet<RegisterCourse> RegisterCourses { get; set; }

        public DbSet<StudentFee> StudentFees { get; set; }

        public DbSet<FeedbackTeacher> FeedbackTeachers { get; set; }

        public DbSet<Schedule> Schedules { get; set; }

        public DbSet<StudentMark> StudentMarks { get; set; }

        public DbSet<Exam> Exams { get; set; }

        public DbSet<StudentForm> StudentForms { get; set; }

        public DbSet<Attendance> Attendances { get; set; }

        public DbSet<Contact> Contacts { get; set; }

    }
}