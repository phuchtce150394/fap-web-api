using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FapWebApi.DTO;
using FapWebApi.Models;

namespace FapWebApi.Models
{

    [Table("TeacherProfile")]
    public class TeacherProfile
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name of Teacher can't be blank.")]
        public string Name { get; set; } = "Teacher Anonymous";

        public string ImageURL { get; set; } = "/media/avatardefault.png";

        public bool Gender { get; set; } = true;

        [Required(ErrorMessage = "RollNumber of Teacher can't be blank.")]
        public string RollNumber { get; set; }

        public DateTime? Birthday { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime DateOfPublic { get; set; } = DateTime.Now;

        //Foreign Key 
        [ForeignKey("Account")]
        public string AccountId { get; set; }
        public Account Account { get; set; }

        public TeacherProfile() {}

        public TeacherProfile(TeacherProfileDTO dto) {

            if(dto.Name != null) Name = dto.Name;
            if(dto.ImageURL != null) ImageURL = dto.ImageURL;
            if(dto.Gender != null) Gender = dto.Gender.Value;
            if(dto.RollNumber != null) RollNumber = dto.RollNumber;
            if(dto.Birthday != null) Birthday = dto.Birthday;
            if(dto.PhoneNumber != null) PhoneNumber = dto.PhoneNumber;
            if(dto.UserId != null) AccountId = dto.UserId;
        }
    }
}