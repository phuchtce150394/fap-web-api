using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FapWebApi.DTO;

namespace FapWebApi.Models
{
    [Table("StudentForm")]
    public class StudentForm 
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Missing StudentRollNumber!")]
        public string StudentRollNumber { get; set; }

        [Required(ErrorMessage = "Missing ApplicationType!")]
        public string ApplicationType { get; set; }

        public string Purpose { get; set; }

        public string FileURL { get; set; }

        public bool isDone { get; set; } = false;

        public StudentForm(){}
        public StudentForm(StudentFormDTO formDTO)
        {
            StudentRollNumber=formDTO.StudentRollNumber;
            ApplicationType=formDTO.ApplicationType;
            Purpose=formDTO.Purpose;
        }

    }

}