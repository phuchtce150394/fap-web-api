using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FapWebApi.DTO;

namespace FapWebApi.Models
{
    [Table("Major")]
    public class Major
    {   
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
    
        [Required]
        [MaxLength(15)]
        public string Code { get; set; }
        
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [ForeignKey("MajorGroup")]
        public int MajorGroupId { get; set; }
        public MajorGroup MajorGroup { get; set; }

        // MappingAllSubjects
        public ICollection<Subject> Subjects { get; set; }

        public Major() {}

        public Major(MajorDTO dto) {
            
            Code=dto.Code;
            Name=dto.Name;
            MajorGroupId=dto.MajorGroupId;
            
        }
        
    }
}