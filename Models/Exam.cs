using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FapWebApi.DTO;

namespace FapWebApi.Models
{
    [Table("Exam")]
    public class Exam 
    {
        [Key]
        public int Id { get; set; }
        
        [Required(ErrorMessage = "Missing CourseId!")]
        public int RegisterCourseId { get; set; }

        public RegisterCourse Course { get; set; }

        [Required(ErrorMessage = "Missing Date!")]
        public DateTime Date { get; set; }
        
        [Required(ErrorMessage = "Missing RoomCode!")]
        public string RoomCode { get; set; }

        [Required(ErrorMessage = "Missing Time!")]
        public string Time { get; set; }

        [Required(ErrorMessage = "Missing ExamForm!")]
        public string ExamForm { get; set; }

        public bool isDone { get; set; } = false;

        public Exam(){}

        public Exam(ExamDTO dto)
        {
            RegisterCourseId=dto.RegisterCourseId;
            Date=dto.Date;
            RoomCode=dto.RoomCode;
            Time=dto.Time;
            ExamForm=dto.ExamForm;
            isDone=dto.isDone;
        }

        public void Update(ExamDTO dto)
        {
            RegisterCourseId=dto.RegisterCourseId;
            Date=dto.Date;
            RoomCode=dto.RoomCode;
            Time=dto.Time;
            ExamForm=dto.ExamForm;
            isDone=dto.isDone;
        }

    }
}

