using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FapWebApi.DTO;

namespace FapWebApi.Models
{
    [Table("AdminProfile")]
    public class AdminProfile
    {
        [Key]
        public int Id {get; set;}

        [Required(ErrorMessage = "Name of Admin profile can't be blank")]
        public string Name { get; set; } = "Admin Anonymous";
        
        public string ImageURL{ get; set;} = "/media/avatardefault.png";

        public bool Gender{get; set;} = true;

        [Required]
        public string RollNumber{get; set;}
        public DateTime? Birthday {get; set;}
        
        [ForeignKey("Account")]
        public string AccountId {get; set;}
        public Account Account { get; set; }

        public AdminProfile() {}

        public AdminProfile(AdminProfileDTO dto) {
            
            if(dto.Name != null) Name=dto.Name;
            if(dto.ImageURL != null) ImageURL=dto.ImageURL;
            if(dto.Gender != null) Gender=dto.Gender.Value;
            if(dto.RollNumber != null) RollNumber=dto.RollNumber;
            if(dto.Birthday != null) Birthday=dto.Birthday;
            if(dto.UserId != null) AccountId=dto.UserId;

        }
        
    }
}