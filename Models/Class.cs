using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FapWebApi.DTO;

namespace FapWebApi.Models
{
    [Table("Class")]
    public class Class
    {
        
        [Key]
        public int Id {get; set;}
        
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        public int Status { get; set; }

        [Required]
        public DateTime DateOfPublic { get; set; } = DateTime.Now;

        [Required]
        [ForeignKey("Semester")]
        public int SemesterId { get; set; }
        public Semester Semester { get; set; }

        public virtual ICollection<StudentProfile> StudentProfiles { get; set; }
        
        public Class()
        {
            StudentProfiles = new HashSet<StudentProfile>();
        }
    }
}