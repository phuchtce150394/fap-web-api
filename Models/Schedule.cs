

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FapWebApi.DTO;

namespace FapWebApi.Models
{   
    [Table("Schedule")]
    public class Schedule 
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Missing ClassName!")]
        public string ClassName { get; set; }

        [Required(ErrorMessage = "Missing SubjectCode!")]
        public string SubjectCode { get; set; }

        [Required(ErrorMessage = "Missing TeacherCode!")]
        public string TeacherCode { get; set; }

        [Required(ErrorMessage = "Missing RoomCode!")]
        public string RoomCode { get; set; }
        public DateTime Date { get; set; }
        [Range(1, 8)]
        public int SlotNo { get; set; }

        public Schedule(){}
        public Schedule(ScheduleDTO dto){

            ClassName=dto.ClassName;
            SubjectCode=dto.SubjectCode;
            TeacherCode=dto.TeacherCode;
            RoomCode=dto.RoomCode;
            Date=dto.Date;
            SlotNo=dto.SlotNo;

        }


    }



}