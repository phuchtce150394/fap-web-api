

namespace FapWebApi.Models
{
    public class StudentFee 
    {

        public int Id { get; set; }
        public string StudentRollNumber { get; set; }
        public string StudentName { get; set; }
        public string SemesterCode { get; set; }
        public int TotalFee { get; set; }
        public int PaidFee { get; set; }
        public bool isComplete { get; set; }
        public string Note { get; set; }

    }
}