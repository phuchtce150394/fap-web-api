using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FapWebApi.DTO;

namespace FapWebApi.Models
{
    [Table("StudentMark")]
    public class StudentMark
    {

        [Key]
        public int Id { get; set; }

        public int RegisterCourseId { get; set; }
        public RegisterCourse Course { get; set; }
        
        [Required(ErrorMessage = "Missing GradeCategory!")]
        public string GradeCategory { get; set; }

        [Required(ErrorMessage = "Missing GradeItem!")]
        public string GradeItem { get; set; }

        [Required(ErrorMessage = "Missing Weight!")]
        public double Weight { get; set; }

        [Required(ErrorMessage = "Missing Value!")]
        public double Value { get; set; }
        public string Comment { get; set; }

        public StudentMark(){}

        public StudentMark(StudentMarkDTO dto)
        {
            RegisterCourseId=dto.RegisterCourseId;
            GradeCategory=dto.GradeCategory;
            GradeItem=dto.GradeItem;
            Weight=dto.Weight;
            Value=dto.Value;
            Comment=dto.Comment;
        }
        
    }

}