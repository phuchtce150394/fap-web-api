using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FapWebApi.Models
{
    [Table("MajorGroup")]
    public class MajorGroup
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set;}

        [Required (ErrorMessage="Code cannot be blank")]
        [MaxLength(15)]
        public string Code { get; set;}

        [Required (ErrorMessage="Name of Group Major cannot be blank")]
        [Display(Name = "Major Group Name", Description = "Name of Group Major")]
        [MaxLength(100)]
        public string Name { get; set;}
        
    }
}