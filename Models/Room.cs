using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FapWebApi.DTO;

namespace FapWebApi.Models
{
    [Table("Room")]
    public class Room
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string RoomCode { get; set; }

        public bool IsActive { get; set; } = true;

        public string Note { get; set; }

        public Room(){}

        public Room(RoomDTO room) {
            Id=room.Id;
            RoomCode=room.RoomCode;
            IsActive=room.IsActive;
            Note=room.Note;
        }


    }
}