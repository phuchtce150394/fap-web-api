using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FapWebApi.Models
{
    [Table("Attendance")]
    public class Attendance
    {
        [Key]
        public int Id { get; set; }

        public int RegisterCourseId { get; set; }
        public RegisterCourse Course { get; set; }

        public int ScheduleId { get; set; }
        public Schedule Schedule { get; set; }

        public bool isPresent { get; set; } = false;

        public string Comment { get; set; }

    }

}