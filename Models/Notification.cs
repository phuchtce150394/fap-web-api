using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using FapWebApi.DTO;

namespace FapWebApi.Models
{
    [Table("Notification")]
    public class Notification
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public String Title { get; set; }
        [Required]
        public String Content { get; set; }
        
        public DateTime DateTimeOfPublic { get; set; } = DateTime.Now;

        public Notification(){}

        public Notification(NotificationDTO dto){
            Title=dto.Title;
            Content=dto.Content;
        }


    }
}