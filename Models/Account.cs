using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
namespace FapWebApi.Models {

    public class Account : IdentityUser
    {

        [Required]
        public override string UserName { get; set; }

        public DateTime DateOfPublic { get; set; } = DateTime.Today;

        // Relationship Fields

        public StudentProfile StudentProfile { get; set; }

        public AdminProfile AdminProfile { get; set; }

        public TeacherProfile TeacherProfile { get; set; }


    }

}