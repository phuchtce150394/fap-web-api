using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FapWebApi.DTO;

namespace FapWebApi.Models {
    [Table("Semester")]
    public class Semester{

        [Key]
        public int Id { get; set; }
        
        [MaxLength(15)]
        public string Code { get; set;}

        [MaxLength(100)]
        public string Name { get; set; }

        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
        
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }

        public Semester(){}

        public Semester(SemesterDTO dto){
            Code=dto.Code;
            Name=dto.Name;
            StartDate=dto.StartDate;
            EndDate=dto.EndDate;
        }
    }
}