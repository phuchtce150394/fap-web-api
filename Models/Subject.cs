using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Collections.Generic;
using FapWebApi.DTO;

namespace FapWebApi.Models
{
    [Table("Subject")]
    public class Subject
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(15)]
        [Required]
        public string Code { get; set; }

        [Required]
        public int Type { get; set; } = 1;

        [Required]
        public string Name { get; set; } 
        
        
        [ForeignKey("Major")]
        public int? MajorId { get; set; }
        public Major Major { get; set; }

        [Required]
        public int TermNo { get; set; }
        
        [Required]
        public int Credit { get; set; } = 0;

        public int Fee { get; set; } = 0;

        public Subject(){}

        public Subject(SubjectDTO dto) {
            
            Code=dto.Code;
            Type=dto.Type;
            Name=dto.Name;
            TermNo=dto.TermNo;
            Credit=dto.Credit;
            Fee=dto.Fee;

        }

        
       
    }
}