using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FapWebApi.DTO;

namespace FapWebApi.Models
{
    [Table("RegisterCourse")]
    public class RegisterCourse
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; } 
        public string StudentRollNumber { get; set; }
        [Required]
        public string SubjectCode { get; set; }
        [Required]
        public string SemesterCode { get; set; }
        public string Status { get; set; }
        public bool isPayment { get; set; } = false;

        public RegisterCourse(){}

        public RegisterCourse(RegisterCourseDTO course) {
            StudentRollNumber=course.StudentRollNumber;
            SubjectCode=course.SubjectCode;
            SemesterCode=course.SemesterCode;
            Status=course.Status;
        }

        // Relation Objects
        public IList<StudentMark> StudentMarks { get; set; }

    }
}