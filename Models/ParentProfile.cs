

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FapWebApi.Models
{
    [Table("ParentProfile")]
    public class ParentProfile
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name of Parent profile can't be blank.")]
        public string Name { get; set; } = "Parrent Anonymous";

        public string Address { get; set; }


        [ForeignKey("StudentProfile")]
        public int StudentProfileId { get; set; }
        public StudentProfile StudentProfile { get; set; }

        //Foreign Key 
        [ForeignKey("Account")]
        public string AccountId { get; set; }
        public Account Account { get; set; }
    }
}