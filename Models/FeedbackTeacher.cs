using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FapWebApi.DTO;

namespace FapWebApi.Models
{
    [Table("FeedbackTeacher")]
    public class FeedbackTeacher
    {
        [Key]
        public int Id { get; set;}
        [Required(ErrorMessage = "Misssing StudentRollNumber!")]
        public string StudentRollNumber { get; set; }
        [Required(ErrorMessage = "Misssing TeacherRollNumber!")]
        public string TeacherRollNumber { get; set; }
        
        [Required(ErrorMessage = "Misssing Content!")]
        public string Content { get; set; }
        public DateTime Created { get; set; } = DateTime.Now;

        public FeedbackTeacher(){}

        public FeedbackTeacher(FeedbackTeacherDTO dto){
            StudentRollNumber=dto.StudentRollNumber;
            TeacherRollNumber=dto.TeacherRollNumber;
            Content=dto.Content;
        }

    }
}