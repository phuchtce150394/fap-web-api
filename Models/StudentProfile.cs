using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FapWebApi.DTO;
using Microsoft.EntityFrameworkCore;

namespace FapWebApi.Models
{
    [Table("StudentProfile")]
    public class StudentProfile
    {
        
        [Key]
        public int Id { get; set; }
        
        [Required(ErrorMessage = "Name of Student profile can't be blank.")]
        public string Name { get; set; } = "Student Anonymous";

        public string ImageURL { get; set; } = "/media/avatardefault.png";

        public DateTime? Birthday { get; set; }

        public bool Gender { get; set; } = true;

        public string IdCard { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "RollNumber of Student can't be blank.")]
        public string RollNumber { get; set; }

        public DateTime EnrollDate { get; set; } = DateTime.Today;

        [ForeignKey("Major")]
        public int? MajorId { get; set; }
        public Major Major { get; set; }

        [ForeignKey("Account")]
        public string AccountId { get; set; }
        public Account Account { get; set; }

        public DateTime DateOfPublic { get; set; } = DateTime.Now;
        public virtual ICollection<Class> Classes { get; set; }
        public StudentProfile()
        {
            Classes = new HashSet<Class>();
        }

        public StudentProfile(StudentProfileDTO dto) {
            
            if(dto.Name != null) Name = dto.Name;
            if(dto.ImageURL != null) ImageURL = dto.ImageURL;
            if(dto.Gender != null) Gender = dto.Gender.Value;
            if(dto.Birthday != null) Birthday = dto.Birthday;
            if(dto.IdCard != null) IdCard = dto.IdCard;
            if(dto.Address != null) Address = dto.Address;
            if(dto.PhoneNumber != null) PhoneNumber = dto.PhoneNumber;
            if(dto.RollNumber != null) RollNumber = dto.RollNumber;
            if(dto.MajorId != null) MajorId = dto.MajorId;
            if(dto.UserId != null) AccountId = dto.UserId;
        }
        
    }
}