using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FapWebApi.Models;
using FapWebApi.DTO;
using FapWebApi.Data;
using FapWebApi.Tools;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace FapWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AttendanceController : ControllerBase
    {
        ApplicationDbContext _context;

        public AttendanceController(ApplicationDbContext context)
        {
            _context = context;
        }

        [NonAction]
        public async Task<Response> UpdateOfStudentCourseAsync(RegisterCourse course)
        {

            var student = await _context.StudentProfiles.Include(item => item.Classes).FirstOrDefaultAsync(item => item.RollNumber==course.StudentRollNumber);
            if(student==null) return new Response {
                Status="Error",
                Message="Student of this course already not exist!"
            };

            var classesOfStudent = student.Classes.Select(item => item.Name).ToList();

            var semester = await _context.Semesters.FirstOrDefaultAsync(item => item.Code==course.SemesterCode);
            if(semester==null) return new Response {
                Status="Error",
                Message="Semester of this course already not exist!"
            };

            var schedulesOfCourse = await _context.Schedules.Where(item => classesOfStudent.Contains(item.ClassName))
                                    .Where(item => item.SubjectCode == course.SubjectCode && (semester.StartDate <= item.Date && item.Date <= semester.EndDate))
                                    .ToListAsync();
            
            foreach(var schedule in schedulesOfCourse)
            {
                var checkExist = await _context.Attendances.FirstOrDefaultAsync(item => item.ScheduleId==schedule.Id && item.RegisterCourseId==course.Id);
                if(checkExist==null)
                {
                    Attendance obj = new Attendance(){RegisterCourseId=course.Id, ScheduleId=schedule.Id};
                    _context.Attendances.Add(obj);
                }
            }

            try {
                await _context.SaveChangesAsync();
            }
            catch {
                return new Response {Status="Error", Message="Faild to add obj. Server handle error!"};
            }

            return new Response { Status="Success", Message=$"Update attendance for course with Id='{course.Id}' done." };
        }

        [HttpGet("GetOfCourse/{CourseId}")]
        public async Task<IActionResult> GetOfCourse(int CourseId)
        {
            var course = await _context.RegisterCourses.FindAsync(CourseId);
            if(course==null) return NotFound(new Response {
                Status="Error",
                Message=$"Course with Id={CourseId} not exist!"
            });

            Response res = await UpdateOfStudentCourseAsync(course);

            if(res.Status=="Error") return StatusCode(StatusCodes.Status500InternalServerError, res);

            var items = await _context.Attendances.Include(item => item.Course).Include(item => item.Schedule)
                        .Where(item => item.RegisterCourseId==CourseId)
                        .OrderBy(item => item.Schedule.Date).ThenBy(item => item.Schedule.SlotNo)
                        .ToListAsync();

            return Ok(items.Select(obj => new AttendanceDTO(obj)));
        }

        [HttpGet("GetOfSchedule/{ScheduleId}")]
        public async Task<IActionResult> GetOfSchedule(int ScheduleId)
        {

            var schedule = await _context.Schedules.FindAsync(ScheduleId);
            if(schedule==null) return NotFound(new Response {
                Status="Error",
                Message=$"Schedule with Id='{ScheduleId}' not exist!"
            });

            var courses = await _context.RegisterCourses.Where(item => item.SubjectCode==schedule.SubjectCode).ToListAsync();
            foreach (var course in courses)
            {
                await UpdateOfStudentCourseAsync(course);
            }

            var items = await _context.Attendances.Include(item => item.Course).Include(item => item.Schedule)
                        .Where(item => item.ScheduleId==ScheduleId)
                        .ToListAsync();

            return Ok(items.Select(obj => new {
                attendance = new AttendanceDTO(obj),
                studentInfo = new StudentProfileDTO(_context.StudentProfiles.FirstOrDefault(item => item.RollNumber==obj.Course.StudentRollNumber))
            }));
        }

        [HttpPut("UpdateStatus/{AttendanceId}")]
        public async Task<IActionResult> UpdateStatusAsync(int AttendanceId, string Comment)
        {
            var checkAttendance = await _context.Attendances.Include(item => item.Course).Include(item => item.Schedule)
                                    .FirstOrDefaultAsync(item => item.Id==AttendanceId);
            if(checkAttendance==null) return NotFound(new Response {
                Status="Error",
                Message=$"Attendance with Id='{AttendanceId}' not exist!"
            });

            checkAttendance.isPresent = true;
            checkAttendance.Comment   = Comment;
            try {
                _context.Attendances.Update(checkAttendance);
                await _context.SaveChangesAsync();
            }
            catch {
                return StatusCode(StatusCodes.Status500InternalServerError, new Response {
                    Status="Error",
                    Message="Server error while handle request. Try again or contact us!"
                });
            }

            return Ok(new AttendanceDTO(checkAttendance));
        }



        
    }
}