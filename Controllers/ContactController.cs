using FapWebApi.Data;
using Microsoft.AspNetCore.Mvc;
using FapWebApi.Models;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System.Linq;
using FapWebApi.Tools;

namespace FapWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public ContactController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var contacts = _context.Contacts.OrderBy(item => item.Id).ToList();
            return Ok(contacts);
        }

        [HttpPost]
        public async Task<IActionResult> Post(Contact contact) 
        {

            if(ModelState.IsValid){

                _context.Contacts.Add(contact);
                try {

                    await _context.SaveChangesAsync();
                }
                catch {
                    return StatusCode(StatusCodes.Status500InternalServerError, new Response {
                        Status="Error",
                        Message="Server error, try later!"
                    });
                }
                
                return Ok(contact);
            }

            return BadRequest();
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> Get(int Id)
        {
            Contact contact = await _context.Contacts.FindAsync(Id);

            if (contact == null) return NotFound(new Response{
                Status="Erorr",
                Message=$"Contact with Id='{Id}' not found!"
            });
            

            return Ok(contact);
        }

        
        [HttpDelete("{Id}")]
        public async Task<IActionResult> Delete(int Id)
        {
            Contact contact = await _context.Contacts.FindAsync(Id);
            if (contact == null) return NotFound(new Response
            {
                Status = "Erorr",
                Message = $"Contact with Id='{Id}' not found!"
            });

            try
            {

                _context.Contacts.Remove(contact);
                await _context.SaveChangesAsync();

                return Ok(new Response
                {
                    Status = "Success",
                    Message = $"Contact has been deleted."
                });
            }
            catch
            {
                return Conflict(new Response
                {
                    Status = "Error",
                    Message = "An error handle while processing data, try again or contact us!"
                });
            }

        }
    }

}