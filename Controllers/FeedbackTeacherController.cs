using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FapWebApi.Models;
using FapWebApi.Data;
using FapWebApi.DTO;
using Microsoft.EntityFrameworkCore;
using FapWebApi.Tools;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace FapWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FeedbackTeacherController : ControllerBase
    {
        ApplicationDbContext _context;
        public FeedbackTeacherController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet("GetTeacherList")]
        public async Task<IActionResult> GetTeacherListAsync(string StudentCode, string SemesterCode)
        {

            var checkStudent = await _context.StudentProfiles.Include(item => item.Classes)
                            .FirstOrDefaultAsync(item => item.RollNumber==StudentCode);
            if(checkStudent==null) return NotFound(new Response{
                Status="Error",
                Message=$"Student with RollNumber='{StudentCode}' not exist!"
            });

            var checkSemester = await _context.Semesters.FirstOrDefaultAsync(item => item.Code==SemesterCode);
            if(checkSemester==null) return NotFound(new Response{
                Status="Error",
                Message=$"Semester with Code='{SemesterCode}' not exist!"
            });

            var classes = checkStudent.Classes.Select(item => item.Name);
            var subjects = await _context.RegisterCourses.Where(item => item.StudentRollNumber==StudentCode && item.SemesterCode==SemesterCode).Select(item => item.SubjectCode).ToListAsync();

            var schedule = await _context.Schedules.Where(item => classes.Contains(item.ClassName) && subjects.Contains(item.SubjectCode)).ToListAsync();

            var teachersCode = schedule.AsEnumerable().GroupBy(item => item.TeacherCode).Select(item => item.Key);

            var teachers = await _context.TeacherProfiles.Where(item => teachersCode.Contains(item.RollNumber)).ToListAsync();

            return Ok(teachers.Select(item => new TeacherProfileDTO(item)));
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync(FeedbackTeacherDTO dto)
        {
            FeedbackTeacher obj = new FeedbackTeacher(dto);

            var context = new ValidationContext(obj);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(obj, context, results);

            if (isValid)
            {

                var checkStudent = await _context.StudentProfiles.FirstOrDefaultAsync(item => item.RollNumber==obj.StudentRollNumber);
                if(checkStudent == null) {
                    return NotFound(new Response {
                        Status="Error",
                        Message=$"Not Found StudentProfile of rollNumber='{obj.StudentRollNumber}'"
                    });
                }

                var checkTeacher = await _context.TeacherProfiles.FirstOrDefaultAsync(item => item.RollNumber==obj.TeacherRollNumber);
                if(checkTeacher == null) {
                    return NotFound(new Response {
                        Status="Error",
                        Message=$"Not Found TeacherProfile of rollNumber='{obj.TeacherRollNumber}'"
                    });
                }
                
                _context.FeedbackTeachers.Add(obj);

                try {
                    await _context.SaveChangesAsync();
                }
                catch {
                    return Conflict(new Response {
                        Status="Error",
                        Message=$"Some fields conflict with database, check again or contact us!"
                    });
                }

                return Ok(new FeedbackTeacherDTO(obj));
                
            }

            var errors = results.Select(item => item.ErrorMessage);

            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });

        }

        [HttpGet]
        public async Task<IActionResult> GetAll(int pageIndex=1, int pageSize=2)
        {
            try {
                var feeds = _context.FeedbackTeachers.OrderBy(item => item.Id);

                var paginatedItems = await PaginatedList<FeedbackTeacher>.CreateAsync(feeds.AsNoTracking(), pageIndex, pageSize);

                if(paginatedItems.TotalPages > 0 && paginatedItems.PageIndex > paginatedItems.TotalPages) return NotFound();

                return Ok(paginatedItems.Select(item => new FeedbackTeacherDTO(item)));
            }
            catch {
                return StatusCode(500, new Response {
                    Status="Error",
                    Message="Server handle error, try again!"
                });
            }
            
        }

        [HttpGet("Filter")]
        public async Task<IActionResult> FilterAsync(string studentRollNumber, string teacherRollNumber, int pageIndex=1, int pageSize=2)
        {
            try {
                var feeds = _context.FeedbackTeachers.OrderBy(item => item.Id)
                            .Where(item => studentRollNumber == null || item.StudentRollNumber == studentRollNumber)
                            .Where(item => teacherRollNumber == null || item.TeacherRollNumber == teacherRollNumber);

                var paginatedItems = await PaginatedList<FeedbackTeacher>.CreateAsync(feeds.AsNoTracking(), pageIndex, pageSize);

                if(paginatedItems.TotalPages > 0 && paginatedItems.PageIndex > paginatedItems.TotalPages) return NotFound();

                return Ok(paginatedItems.Select(item => new FeedbackTeacherDTO(item)));
            }
            catch {
                return StatusCode(500, new Response {
                    Status="Error",
                    Message="Server handle error, try again!"
                });
            }
        }

        [HttpDelete("{Id}")]
        public async Task<IActionResult> DeleteAsync(int Id)
        {
            FeedbackTeacher obj = await _context.FeedbackTeachers.FindAsync(Id);
            if(obj==null) return NotFound(new Response{
                Status="Error",
                Message=$"FeedBack with Id='{Id}' not found!"
            });

            _context.FeedbackTeachers.Remove(obj);
            try {
                await _context.SaveChangesAsync();
            }
            catch {
                return Conflict(new Response{
                    Status="Error",
                    Message="Can't delete this object or an error handle while processing request, try again or contact us!"
                });
            }

            return Ok(new {
                Status="Success",
                Message=$"Deleted FeedBack with Id='{Id}'",
                data= new FeedbackTeacherDTO(obj)
            });
        }
    }
}