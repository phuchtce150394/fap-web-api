using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FapWebApi.Data;
using FapWebApi.Models;
using FapWebApi.DTO;
using FapWebApi.Tools;
using System.ComponentModel.DataAnnotations;

namespace FapWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public RoomController(ApplicationDbContext context)
        {
            _context = context;
        }

        
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var items = await _context.Rooms.Select(item => new RoomDTO(item)).ToListAsync();
            return Ok(items);
        }

        
        [HttpGet("{Id}")]
        public async Task<IActionResult> Get(int Id)
        {
            var obj = await _context.Rooms.FindAsync(Id);

            if (obj == null) return NotFound(new Response{
                Status="Erorr",
                Message=$"Room with Id='{Id}' not found!"
            });
            

            return Ok(new RoomDTO(obj));
        }


        [HttpPut("{Id}")]
        public async Task<IActionResult> Put(int Id, RoomDTO dto)
        {
            
            Room room = await _context.Rooms.FindAsync(Id);
            if (room == null) return NotFound(new Response{
                Status="Erorr",
                Message=$"Room with Id='{Id}' not found!"
            });

            var checkCode = await _context.Rooms.AnyAsync(item => !item.RoomCode.Equals(room.RoomCode) && item.RoomCode == dto.RoomCode);
            if(checkCode) return Conflict(new Response{
                Status="Error",
                Message=$"RoomCode '{dto.RoomCode}' already existed!"
            });

            room.RoomCode=dto.RoomCode;
            room.IsActive=dto.IsActive;
            room.Note=dto.Note;

            var context = new ValidationContext(room);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(room, context, results);

            if (isValid)
            {
                
                _context.Rooms.Update(room);

                try {
                    await _context.SaveChangesAsync();
                }
                catch {
                    return Conflict(new Response {
                        Status="Error",
                        Message=$"Some fields conflict with database, check again or contact us!"
                    });
                }

                return Ok(new RoomDTO(room));
                
            }

            var errors = results.Select(item => item.ErrorMessage);

            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });
            
        }

        [HttpPost]
        public async Task<IActionResult> Post(RoomDTO dto)
        {

            Room room = new Room(dto);

            var checkCode = await _context.Rooms.AnyAsync(item => item.RoomCode == dto.RoomCode);
            if(checkCode) return Conflict(new Response{
                Status="Error",
                Message=$"RoomCode '{dto.RoomCode}' already existed!"
            });

            var context = new ValidationContext(room);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(room, context, results);

            if (isValid)
            {
                
                _context.Rooms.Add(room);

                try {
                    await _context.SaveChangesAsync();
                }
                catch {
                    return Conflict(new Response {
                        Status="Error",
                        Message=$"Some fields conflict with database, check again or contact us!"
                    });
                }

                return Ok(new RoomDTO(room));
                
            }

            var errors = results.Select(item => item.ErrorMessage);

            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });
        }

        [HttpDelete("{Id}")]
        public async Task<IActionResult> Delete(int Id)
        {
            Room room = await _context.Rooms.FindAsync(Id);
            if (room == null) return NotFound(new Response{
                Status="Erorr",
                Message=$"Room with Id='{Id}' not found!"
            });

            try {

                _context.Rooms.Remove(room);
                await _context.SaveChangesAsync();

                return Ok(new Response {
                    Status="Success",
                    Message=$"Room has been deleted."
                });
            }
            catch {
                return Conflict(new Response {
                    Status="Error",
                    Message="An error handle while processing data, try again or contact us!"
                });
            }

        }

        [HttpGet("IsEmpty")]
        public async Task<IActionResult> IsEmptyAsync(string roomCode, DateTime date, int slotNo)
        {

            var checkRoom = await _context.Rooms.FirstOrDefaultAsync(item => item.RoomCode==roomCode);
            if(checkRoom == null) return Conflict(new Response {
                Status="Error",
                Message=$"RoomCode '{roomCode}' invalid!"
            });

            var checkSlot = slotNo <= 8 && slotNo > 0;
            if(checkSlot == false) return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message="SlotNo only accept 1-8!"
            });

            var checkSchedule = await _context.Schedules.FirstOrDefaultAsync(item => item.RoomCode==roomCode && item.SlotNo==slotNo);
            if(checkSchedule != null) return Ok(new {IsEmpty=false});
            
            return Ok(new {IsEmpty=true});
        }

        [NonAction]
        public static async Task<bool> IsEmptyAsync(ApplicationDbContext context, string roomCode, DateTime date, int slotNo)
        {
            var checkRoom = await context.Rooms.FirstOrDefaultAsync(item => item.RoomCode==roomCode);
            if(checkRoom == null) return false;

            var checkSlot = slotNo <= 8 && slotNo > 0;
            if(checkSlot == false) return false;

            var checkSchedule = await context.Schedules.FirstOrDefaultAsync(item => item.RoomCode==roomCode && item.SlotNo==slotNo);
            if(checkSchedule != null) return false;
            
            return true;
        }


    }
}
