using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using FapWebApi.Data;
using FapWebApi.Identity;
using FapWebApi.Models;
using FapWebApi.Tools;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace FapWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {

        ApplicationDbContext _context;
        UserManager<Account> _userManager;
        RoleManager<IdentityRole> _roleManager;

        public AccountController(UserManager<Account> userManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context)
        {
            _userManager   = userManager;
            _roleManager   = roleManager;
            _context       = context;
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register(String username="", String email="", String password="")  
        {  
            Account userExists = await _userManager.FindByNameAsync(username);
            if (userExists != null) {

                return StatusCode(
                    StatusCodes.Status409Conflict,
                    new Response { Status = "Error", Message = $"User {username} already exists!"
                });
            }
  
            Account user = new Account() {Email = email, UserName = username, SecurityStamp = Guid.NewGuid().ToString()};

            var result = await _userManager.CreateAsync(user, password);

            if (!result.Succeeded) {
                var errors = result.Errors.Select(item => item.Description).ToList();
                return StatusCode(
                    StatusCodes.Status500InternalServerError,
                    new Response { Status = "Error", Message = errors.FirstOrDefault()
                });
            }

            await UserRoles.CreateRolesAsync(_userManager, _roleManager);
            
            return Ok(new Response { Status = "Success", Message = $"User '{username}' created successfully!" });

        }

        [HttpGet]
        public async Task<IActionResult> GetAll(int pageIndex=1, int pageSize=2)
        {
            try {

                var users = _userManager.Users.OrderBy(item => item.UserName);

                var paginatedUsers = await PaginatedList<Account>.CreateAsync(users.AsNoTracking(), pageIndex, pageSize);

                if(paginatedUsers.TotalPages > 0 && paginatedUsers.PageIndex > paginatedUsers.TotalPages) return NotFound();

                return Ok(paginatedUsers.Select(user => new {
                    id=user.Id,
                    username=user.UserName,
                    email=user.Email,
                    role=_userManager.GetRolesAsync(user).Result,
                    created=user.DateOfPublic
                }));
            }
            catch {
                return StatusCode(500, new Response {
                    Status="Error",
                    Message="Server handle error, try again!"
                });
            }
        }



        [HttpGet]
        [Route("{userId}")]
        public async Task<IActionResult> GetInfo(string userId)
        {
            if(userId != null) {
                Account item = await _userManager.FindByIdAsync(userId);
                if(item == null) return NotFound(new Response {Status="Error", Message=$"Not found account info with userId='{userId}'"});
                return Ok(new {
                    id=item.Id,
                    username=item.UserName,
                    email=item.Email,
                    role=_userManager.GetRolesAsync(item).Result,
                    created=item.DateOfPublic
                });
            }
            else {
                return BadRequest(new Response {Status="Error", Message="Missing userId!"});
            }

        }


        [HttpDelete]
        [Route("{userId}")]
        public async Task<IActionResult> DeleteAsync(string userId)
        {

            if (userId == null) {
                return BadRequest(new Response {Status="Error", Message="Missing userId!"});
            }

            Account user = await _userManager.FindByIdAsync(userId);
            if(user == null) {
                return NotFound(new Response {
                    Status="Error",
                    Message=$"Not found user with id='{userId}'"
                });
            }

            try {

                await _userManager.DeleteAsync(user);
            }
            catch {
                return Conflict(new Response {
                    Status="Error",
                    Message="Failed to delete account because relationship data. Check again"
                });
            }

            return Ok(new {
                message = $"User '{userId}' has been deleted."
            });
        }

        [HttpPut]
        [Route("AddRole")]
        public async Task<IActionResult> AddRole(string userId, string role)
        {
            Account user = await _userManager.FindByIdAsync(userId);
            if(user == null) return StatusCode(StatusCodes.Status404NotFound, new Response {Status="Error", Message=$"UserId {userId} not found."});
            List<String> roles = _roleManager.Roles.Select(item => item.Name).ToList();
            if(roles.Contains(role)) {

                var userRoles = await _userManager.GetRolesAsync(user);
                if(userRoles.Contains(role)) {
                    return Ok(new Response {
                        Status="Warning",
                        Message=$"'{user.UserName}' with Id '{userId}' already has role '{role}'"});
                }
                
                await _userManager.AddToRoleAsync(user, role);
                return Ok(new Response {Status="Success", Message=$"User '{user.UserName}' with id '{userId}' add to role '{role}' success."});
            } else {
                String StringRoles = roles[0];
                for(int i = 1; i < roles.Count; i++) {
                    StringRoles += $", {roles[i]}";
                }
                return StatusCode(
                    StatusCodes.Status400BadRequest,
                    new Response {Status="Error", Message=$"Role only accept {StringRoles}. Not '{role}'!"});
            }
        }

        [HttpPut]
        [Route("RemoveRole")]
        public async Task<IActionResult> RemoveRoleAsync(string userId, string role) {

            Account user = await _userManager.FindByIdAsync(userId);
            if(user == null) return StatusCode(StatusCodes.Status404NotFound, new Response {Status="Error", Message=$"UserId {userId} not found."});
            List<String> roles = _roleManager.Roles.Select(item => item.Name).ToList();
            if(roles.Contains(role)) {

                var userRoles = await _userManager.GetRolesAsync(user);
                if(!userRoles.Contains(role)) {
                    return Ok(new Response {
                        Status="Warning",
                        Message=$"'{user.UserName}' with Id '{userId}' don't have role '{role}'"});
                }
                
                await _userManager.RemoveFromRoleAsync(user, role);
                return Ok(new Response {Status="Success", Message=$"User '{user.UserName}' with id '{userId}' removed role '{role}' success."});
            } else {
                String StringRoles = roles[0];
                for(int i = 1; i < roles.Count; i++) {
                    StringRoles += $", {roles[i]}";
                }
                return StatusCode(
                    StatusCodes.Status400BadRequest,
                    new Response {Status="Error", Message=$"Role only accept {StringRoles}. Not '{role}'!"});
            }

        }

        [HttpPut("UpdatePassword")]
        public async Task<IActionResult> UpdatePassword(UpdatePwdModel data) {

            var checkAccount = await _userManager.FindByEmailAsync(data.Email);
            if(checkAccount == null) return BadRequest(new Response{
                Status="Error",
                Message="Email or Password incorrect. Check again."
            });

            var checkPassword = await _userManager.CheckPasswordAsync(checkAccount, data.OldPassword);
            if(checkPassword == false) return BadRequest(new Response{
                Status="Error",
                Message="Email or Password incorrect. Check again."
            });

            var checkConfirm = data.NewPassword.Equals(data.ConfirmPassword);
            if(checkConfirm == false) return BadRequest(new Response{
                Status="Error",
                Message="NewPassword and ConfirmPassword not match. Check again."
            });

            var result = await _userManager.ChangePasswordAsync(checkAccount, data.OldPassword, data.NewPassword);

            if(result.Succeeded) return Ok(new Response{
                Status="Success",
                Message=$"Changed password for user '{checkAccount.UserName}' successfully."
            });

            return StatusCode(StatusCodes.Status503ServiceUnavailable, new Response {
                Status="Error",
                Message="Can't update password! Unknown the reason!"
            });

        }

    }
}