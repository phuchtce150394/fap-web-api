using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using FapWebApi.Data;
using FapWebApi.DTO;
using FapWebApi.Identity;
using FapWebApi.Models;
using FapWebApi.Tools;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FapWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeacherProfileController : ControllerBase
    {
        ApplicationDbContext _context;
         UserManager<Account> _userManager;


        public TeacherProfileController(ApplicationDbContext context, UserManager<Account> userManager)
        {
            _context = context;
            _userManager = userManager;
        }



        [HttpGet]
        // GET: api/TeacherProfile
        public async Task<IActionResult> GetAllAsync(int pageIndex=1, int pageSize=2)
        {
            try {
                var teachers = _context.TeacherProfiles.OrderBy(item => item.Name);

                var paginatedTeachers = await PaginatedList<TeacherProfile>.CreateAsync(teachers.AsNoTracking(), pageIndex, pageSize);

                if(paginatedTeachers.TotalPages > 0 && paginatedTeachers.PageIndex > paginatedTeachers.TotalPages) return NotFound();

                return Ok(paginatedTeachers.Select(profile => new TeacherProfileDTO(profile)));
            }
            catch {
                return StatusCode(500, new Response {
                    Status="Error",
                    Message="Server handle error, try again!"
                });
            }
        }


        [HttpGet("Id")]
        public async Task<IActionResult> Get(int Id)
        {
            TeacherProfile profile = await _context.TeacherProfiles.FindAsync(Id);

            if (profile == null) return NotFound(new Response { Status = "Error", Message = $"Can not find Profile of Teacher with this Id '{Id}'" });

            return Ok(new TeacherProfileDTO(profile));
        }

        [HttpGet("GetByUserId")]
        public async Task<IActionResult> GetByUserId(string userId)
        {
            TeacherProfile profile = await _context.TeacherProfiles.FirstOrDefaultAsync(item => item.AccountId == userId);

            if (profile == null) return NotFound(new Response { Status = "Error", Message = $"Can not find Profile of Teacher with this userId '{userId}'" });

            return Ok(new TeacherProfileDTO(profile));
        }

        [HttpGet("GetByRollNumber")]
        public async Task<IActionResult> GetByRollNumberAsync(string rollNumber)
        {
            TeacherProfile profile = await _context.TeacherProfiles.FirstOrDefaultAsync(item => item.RollNumber == rollNumber);

            if (profile == null) return NotFound(new Response { Status = "Error", Message = $"Can not find Profile of Teacher with this rollNumber '{rollNumber}'" });

            return Ok(new TeacherProfileDTO(profile));
        }

        [HttpPut]
        public async Task<IActionResult> Update(TeacherProfileDTO dto)
        {
            TeacherProfile profile = await _context.TeacherProfiles.FindAsync(dto.Id);

            if (profile == null)
            {
                return NotFound(new Response { Status = "Error", Message = $"Can not find existed profile with this ID'{dto.Id}'" });
            }

            if(dto.Name != null) profile.Name = dto.Name;
            if(dto.ImageURL != null) profile.ImageURL = dto.ImageURL;
            if(dto.Gender != null) profile.Gender = dto.Gender.Value;
            if(dto.RollNumber != null) profile.RollNumber = dto.RollNumber;
            if(dto.Birthday != null) profile.Birthday = dto.Birthday;
            if(dto.PhoneNumber != null) profile.PhoneNumber = dto.PhoneNumber;
            if(dto.UserId != null) profile.AccountId = dto.UserId;

            var context = new ValidationContext(profile);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(profile, context, results);

            if (isValid)
            {
                _context.TeacherProfiles.Update(profile);
                try {

                    await _context.SaveChangesAsync();
                    return Ok(new TeacherProfileDTO(profile));
                }
                catch {
                    return Conflict(new Response {
                        Status="Error",
                        Message="Some errors with data, check again!"
                    });
                }
            }

            var errors = results.Select(item => item.ErrorMessage);
            return StatusCode(StatusCodes.Status406NotAcceptable, new Response
            {
                Status = "Error",
                Message = errors.FirstOrDefault()
            });

        }

        [HttpDelete("{Id}")]
        public async Task<IActionResult> Delete(int Id)
        {

            TeacherProfile profile = await _context.TeacherProfiles.FindAsync(Id);

            if (profile == null)
            {
                return NotFound(new Response
                {
                    Status = "Error",
                    Message = $"TeacherProfile with id='{Id}' does not exist."
                });
            }

            
            try
            {
                _context.TeacherProfiles.Remove(profile);
                await _context.SaveChangesAsync();
            }
            catch
            {
                return Conflict(new Response
                {
                    Status = "Error",
                    Message = "Can not delete this profile!"
                });
            }

            return Ok(new Response
            {
                Status = "Success",
                Message = $"TeacherProfile of '{profile.Name}' deleted."
            });

        }

    }
}