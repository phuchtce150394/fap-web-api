using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using FapWebApi.Models;
using FapWebApi.Data;
using Microsoft.AspNetCore.Identity;
using System;

namespace FapWebApi.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {

        ApplicationDbContext _context;
        UserManager<Account> _userManager;

        public TestController(ApplicationDbContext context, UserManager<Account> userManager)
        {
            _context     = context;
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult Get(DateTime StartDate)
        {   
            string[] Day = new string[] {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

            TimeSpan oneDay = new TimeSpan(1, 0, 0, 0);
            for(DateTime current = StartDate; current <= StartDate + 10 * oneDay; current += oneDay) {
                System.Console.WriteLine(current.DayOfWeek);
            }

            return Ok(new {
                test=Day[(int) StartDate.DayOfWeek]
            });
        }
        
    }
}