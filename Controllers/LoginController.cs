using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using FapWebApi.Data;
using FapWebApi.Identity;
using FapWebApi.Models;
using Google.Apis.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using static Google.Apis.Auth.GoogleJsonWebSignature;

namespace FapWebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LoginController : ControllerBase
    {

        private readonly UserManager<Account> _userManager;
        private readonly IConfiguration _configuration;

        private readonly ApplicationDbContext _context;

        public LoginController(UserManager<Account> userManager, IConfiguration configuration, ApplicationDbContext context)
        {
            _userManager = userManager;
            _configuration = configuration;
            _context = context;
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginModel Data) {  

            Account user = await _userManager.FindByNameAsync(Data.UserName);
            if (user != null && await _userManager.CheckPasswordAsync(user, Data.Password)) { 

                var userRoles = await _userManager.GetRolesAsync(user);  
  
                var authClaims = new List<Claim>  
                {  
                    new Claim(ClaimTypes.Name, user.UserName),  
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),  
                };  
  
                foreach (var userRole in userRoles)  
                {  
                    authClaims.Add(new Claim(ClaimTypes.Role, userRole));  
                }   
                
                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));  
  
                var token = new JwtSecurityToken(issuer: _configuration["JWT:ValidIssuer"],
                    audience: _configuration["JWT:ValidAudience"],
                    expires: DateTime.Now.AddHours(3),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256));  
  
                return Ok(new
                {  
                    token = new JwtSecurityTokenHandler().WriteToken(token),  
                    expiration = token.ValidTo  
                });
            };
            
            return Unauthorized();
        }

        [NonAction]
        public async Task<Account> Authenticate(Payload payload)
        {
            await Task.Delay(1);
            return await FindUserOrAddAsync(payload);
        }

        [NonAction]
        private async Task<Account> FindUserOrAddAsync(Payload payload)
        {
            Account user = await _userManager.FindByEmailAsync(payload.Email);

            if (user == null)
            {
                user = new Account()
                {
                    UserName = payload.Email,
                    Email = payload.Email,
                };
                await _userManager.CreateAsync(user);
                await _userManager.AddToRoleAsync(user, UserRoles.Student);
            }
            
            return user;
        }


        [HttpPost("Google")]
        public async Task<IActionResult> Google([FromBody] UserGoogleView userGoogleView)
        {
            try
            {
    
                Payload payload = GoogleJsonWebSignature.ValidateAsync(userGoogleView.tokenId, new GoogleJsonWebSignature.ValidationSettings()).Result;

                var user = await Authenticate(payload);

                var userRoles = await _userManager.GetRolesAsync(user);  
  
                var authClaims = new List<Claim>  
                {  
                    new Claim(ClaimTypes.Name, user.UserName),  
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),  
                };  
  
                foreach (var userRole in userRoles)  
                {  
                    authClaims.Add(new Claim(ClaimTypes.Role, userRole));  
                }   
                
                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));  
  
                var token = new JwtSecurityToken(issuer: _configuration["JWT:ValidIssuer"],
                    audience: _configuration["JWT:ValidAudience"],
                    expires: DateTime.Now.AddHours(24),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256));  
  
                return Ok(new
                {  
                    token = new JwtSecurityTokenHandler().WriteToken(token),  
                    expiration = token.ValidTo  
                });
            }
            catch (Exception ex)
            {
                BadRequest(ex.Message);
            }

            return BadRequest();
            
        }

        [HttpGet("LoginAccount")]
        [Authorize]
        public async Task<IActionResult> LoginAccount() {
            Account user = await _userManager.FindByNameAsync(User.Identity.Name);
            return Ok(new {
                    id=user.Id,
                    username=user.UserName,
                    email=user.Email,
                    role=_userManager.GetRolesAsync(user).Result,
                    created=user.DateOfPublic
                });
        }

    }
}