using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FapWebApi.Data;
using FapWebApi.Models;
using FapWebApi.Tools;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using FapWebApi.DTO;
using System.ComponentModel.DataAnnotations;

namespace FapWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MajorController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public MajorController(ApplicationDbContext context)
        {
            _context = context;
        }

        
        [HttpGet]
        public async Task<IActionResult> Get(string majorGroupCode)
        {

            var majors = _context.Majors.AsQueryable();

            if(majorGroupCode != null) majors = majors.Where(major => major.MajorGroup.Code == majorGroupCode);

            return Ok(await majors.OrderBy(major => major.MajorGroupId).Select(major => new MajorDTO(major)).ToListAsync());
        }


        [HttpGet("{Id}")]
        public async Task<ActionResult> Get(int Id)
        {
            
            Major major = await _context.Majors.Include(item => item.MajorGroup).Include(item => item.Subjects.OrderBy(subject => subject.TermNo)).FirstOrDefaultAsync(item => item.Id == Id);

            if(major == null) return NotFound(new Response {
                Status="Error",
                Message=$"Not found major with id='{Id}'"
            });

            return Ok(new MajorDTO(major));
        }

        [HttpGet("GetByCode")]
        public async Task<ActionResult> GetByCode(string majorCode)
        {
            
            Major major = await _context.Majors.Include(item => item.MajorGroup).Include(item => item.Subjects.OrderBy(subject => subject.TermNo)).FirstOrDefaultAsync(item => item.Code == majorCode);

            if(major == null) return NotFound(new Response {
                Status="Error",
                Message=$"Not found major with Code='{majorCode}'"
            });

            return Ok(new MajorDTO(major));
        }


        [HttpPut("{Id}")]
        public async Task<IActionResult> Put(int Id, MajorDTO dto)
        {

            Major major = await _context.Majors.FindAsync(Id);
            if(major == null) return NotFound(new Response {
                Status="Error",
                Message=$"Not found major with Id='{Id}'"
            });

            var checkMajorGroupExist = await _context.MajorGroups.FirstOrDefaultAsync(item => item.Id == dto.MajorGroupId);

            if (checkMajorGroupExist == null)
            {
                return BadRequest(new Response
                {
                    Status = "Error",
                    Message = "MajorGroup is not exist or not match!"
                });
            }

            var checkMajorCode = await _context.Majors.AnyAsync(item => !item.Code.Equals(major.Code) && item.Code == dto.Code);
            if(checkMajorCode) {
                return Conflict(new Response {
                    Status="Error",
                    Message=$"Major Code {dto.Code} already existed!"
                });
            }

            major.Code=dto.Code;
            major.Name=dto.Name;
            major.MajorGroupId=dto.MajorGroupId;

            var context = new ValidationContext(major);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(major, context, results);

            if (isValid)
            {
                
                _context.Majors.Update(major);

                try {
                    await _context.SaveChangesAsync();
                }
                catch {
                    return Conflict(new Response {
                        Status="Error",
                        Message=$"Some fields conflict with database, check again or contact us!"
                    });
                }
                major.MajorGroup = checkMajorGroupExist;
                return Ok(new MajorDTO(major));
                
            }

            var errors = results.Select(item => item.ErrorMessage);

            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });

        }


        [HttpPost]
        public async Task<IActionResult> Post(MajorDTO dto)
        {
            var checkMajorGroupExist = await _context.MajorGroups.FirstOrDefaultAsync(item => item.Id == dto.MajorGroupId);

            if (checkMajorGroupExist == null)
            {
                return BadRequest(new Response
                {
                    Status = "Error",
                    Message = "MajorGroup is not exist or not match!"
                });
            }

            var checkMajorCode = await _context.Majors.AnyAsync(item => item.Code == dto.Code);
            if(checkMajorCode) {
                return Conflict(new Response {
                    Status="Error",
                    Message=$"Major Code {dto.Code} already existed!"
                });
            }

            Major major = new Major(dto);
            
            var context = new ValidationContext(major);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(major, context, results);

            if (isValid)
            {
                
                _context.Majors.Add(major);

                try {
                    await _context.SaveChangesAsync();
                }
                catch {
                    return Conflict(new Response {
                        Status="Error",
                        Message=$"Some fields conflict with database, check again or contact us!"
                    });
                }
                major.MajorGroup = checkMajorGroupExist;
                return Ok(new MajorDTO(major));
                
            }

            var errors = results.Select(item => item.ErrorMessage);

            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });

        }

       
        [HttpDelete("{Id}")]
        public async Task<IActionResult> Delete(int Id)
        {
            var major = await _context.Majors.FindAsync(Id);
            if (major == null) return NotFound(new Response{
                Status="Error",
                Message=$"Major with Id='{Id}' not found!"
            });
            
            try {

                _context.Majors.Remove(major);
                await _context.SaveChangesAsync();
            }
            catch {
                return Conflict(new Response{
                    Status="Error",
                    Message=$"Can't delete major '{major.Code}' because a lot of data relationship. Check again or contact us!"
                });
            }

            return Ok(new Response{
                Status="Success",
                Message=$"Delete major '{major.Code}' success."
            });
        }

    }
}
