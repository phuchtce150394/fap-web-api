

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FapWebApi.Models;
using FapWebApi.Data;
using FapWebApi.DTO;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.ComponentModel.DataAnnotations;
using FapWebApi.Tools;
using Microsoft.EntityFrameworkCore;

namespace FapWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentFormController : ControllerBase
    {
        ApplicationDbContext _context;
        public StudentFormController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Get(string StudentCode, string ApplicationType, bool? isDone)
        {

            var items = await _context.StudentForms.Where(item => (StudentCode == null || item.StudentRollNumber==StudentCode) 
                                                    && (ApplicationType == null || ApplicationType==item.ApplicationType) 
                                                    && (isDone==null || isDone==item.isDone)).ToListAsync();

            return Ok(items.Select(item => new StudentFormDTO(item)));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromForm] StudentFormDTO formDTO, [FromForm] IFormFile formFile)
        {
            StudentForm stdForm = new StudentForm(formDTO);

            var context = new ValidationContext(stdForm);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(stdForm, context, results);

            if (isValid)
            {

                var checkStudent = await _context.StudentProfiles.FirstOrDefaultAsync(item => item.RollNumber==formDTO.StudentRollNumber);
                if(checkStudent==null) return BadRequest(new Response{
                    Status="Error",
                    Message=$"Student with RollNumber='{formDTO.StudentRollNumber}' not exist!"
                });
                
                if(formFile==null) return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                    Status="Error",
                    Message="Missing file!"
                });

                var checkFileType = Path.GetExtension(formFile.FileName);
                if(checkFileType != ".docx") return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                    Status="Error",
                    Message="Only accept '.docx' file!"
                });
                
                stdForm.FileURL = Save(formFile);

                try {
                    _context.StudentForms.Add(stdForm);
                    await _context.SaveChangesAsync();
                }
                catch {
                    return Conflict(new Response {
                        Status="Error",
                        Message=$"An error occur while handle request, try again or contact us!"
                    });
                }

                return Ok(new StudentFormDTO(stdForm));
                
            }

            var errors = results.Select(item => item.ErrorMessage);

            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });

        }


        [NonAction]
        public String Save(IFormFile File) {

            string Name = $"form_{Guid.NewGuid()}" + Path.GetExtension(File.FileName);
            
            //Get url To Save
            string RelativeSaveDirectory = $"/media/student-forms/";
            string AbsoluteSaveFullPath = Directory.GetCurrentDirectory().Replace("\\", "/") + "/wwwroot" + RelativeSaveDirectory + Name;

            if(Directory.Exists(Path.GetDirectoryName(AbsoluteSaveFullPath)) == false) {
                Directory.CreateDirectory(Path.GetDirectoryName(AbsoluteSaveFullPath));
            }
            using (FileStream stream = new FileStream(AbsoluteSaveFullPath, FileMode.Create))
            {
                File.CopyTo(stream);
            }

            return $"{RelativeSaveDirectory}{Name}";
        }

        [HttpGet("Download/{Id}")]
        public async Task<IActionResult> Download(int Id) {
            
            var stdForm = await _context.StudentForms.FindAsync(Id);
            if(stdForm==null) return NotFound(new Response{
                Status="Error",
                Message=$"ApplicationForm with Id='{Id}' not exist!"
            });


            string AbsoluteSaveFullPath = Directory.GetCurrentDirectory().Replace("\\", "/") + "/wwwroot" + stdForm.FileURL;
            if(System.IO.File.Exists(AbsoluteSaveFullPath) == false)
            {
                return NotFound();
            }

            Stream stream = new FileStream(AbsoluteSaveFullPath, FileMode.Open);

            if(stream == null) return NotFound(new Response{
                Status="Error",
                Message="File not exist in system!"
            }); 

            return File(stream, "application/octet-stream", $"student-form-{Id}.docx");
        }
        
        [HttpPut("MakeComplete/{Id}")]
        public async Task<IActionResult> MakeCompleteAsync(int Id)
        {
            var stdForm = await _context.StudentForms.FindAsync(Id);
            if(stdForm==null) return NotFound(new Response{
                Status="Error",
                Message=$"ApplicationForm with Id='{Id}' not exist!"
            });
            
            stdForm.isDone = true;
            _context.StudentForms.Update(stdForm);

            try {
                await _context.SaveChangesAsync();
            }
            catch {
                return Conflict(new Response{
                    Status="Error",
                    Message="An error while handle request. Try again or contact us!"
                });
            }

            return Ok(new StudentFormDTO(stdForm));
        }

        [HttpDelete("{Id}")]
        public async Task<IActionResult> Delete(int Id)
        {
            var stdForm = await _context.StudentForms.FindAsync(Id);
            if(stdForm==null) return NotFound(new Response{
                Status="Error",
                Message=$"ApplicationForm with Id='{Id}' not exist!"
            });
            
            _context.StudentForms.Remove(stdForm);

            try {
                await _context.SaveChangesAsync();
            }
            catch {
                return Conflict(new Response{
                    Status="Error",
                    Message="An error while handle request. Try again or contact us!"
                });
            }

            return Ok(new StudentFormDTO(stdForm));
        }
    }
}