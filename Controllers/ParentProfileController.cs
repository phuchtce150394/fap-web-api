using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FapWebApi.Data;
using FapWebApi.Models;
using FapWebApi.Identity;
using System.ComponentModel.DataAnnotations;
using FapWebApi.Tools;
using FapWebApi.DTO;

namespace FapWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParentProfileController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public ParentProfileController(ApplicationDbContext context)
        {
            _context = context;
        }

        
        [HttpGet]
        public async Task<IActionResult> GetAll(int pageIndex=1, int pageSize=2)
        {

            try {
                var parents = _context.ParentProfiles.Include(item => item.StudentProfile).OrderBy(item => item.Name);

                var paginatedParents = await PaginatedList<ParentProfile>.CreateAsync(parents.AsNoTracking(), pageIndex, pageSize);

                if(paginatedParents.TotalPages > 0 && paginatedParents.PageIndex > paginatedParents.TotalPages) return NotFound();

                return Ok(paginatedParents.Select(profile => new ParentProfileDTO(profile)));
            }
            catch {
                return StatusCode(500, new Response {
                    Status="Error",
                    Message="Server handle error, try again!"
                });
            }

        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> Get(int Id) {

            var profile = await _context.ParentProfiles.Include(item => item.StudentProfile).FirstOrDefaultAsync(item => item.Id == Id);

            if(profile == null) return NotFound(new Response {
                Status="Error",
                Message=$"No have any ParentProfile with id='{Id}'"
            });

            return Ok(new {
                Id = profile.Id,
                UserId = profile.AccountId,
                Name = profile.Name,
                Address = profile.Address,
                StudentProfileId = profile.StudentProfileId,
                StudentProfile = new StudentProfileDTO(profile.StudentProfile)
            });

        }


        [HttpGet("GetByUserId")]
        public async Task<IActionResult> GetByUserId(string userId)
        {
            var profile = await _context.ParentProfiles.Include(item => item.StudentProfile).Where(profile => profile.AccountId == userId).FirstOrDefaultAsync();

            if(profile == null) return NotFound(new Response {
                Status="Error",
                Message=$"No have any ParentProfile with UserId='{userId}'"
            });

            return Ok(new {
                Id = profile.Id,
                UserId = profile.AccountId,
                Name = profile.Name,
                Address = profile.Address,
                StudentProfileId = profile.StudentProfileId,
                StudentProfile = new StudentProfileDTO(profile.StudentProfile)
            });
        }

        
        [HttpPut]
        public async Task<IActionResult> UpdateParentProfile(ParentProfileDTO profileView)
        {
            ParentProfile profile = await _context.ParentProfiles.Include(item => item.StudentProfile).FirstOrDefaultAsync(item => item.Id == profileView.Id);
            if (profile == null)
            {
                return NotFound(new Response { Status = "Error", Message = $"No have any ParentProfile with Id '{profileView.Id}'" });
            }

            if (profileView.Name != null) profile.Name = profileView.Name;
            if (profileView.Address != null) profile.Address = profileView.Address;
            if (profileView.StudentProfileId != 0) {
                var student = await _context.StudentProfiles.FindAsync(profileView.StudentProfileId);
                if(student == null) return Conflict(new Response {
                    Status = "Error",
                    Message = $"Student with id='{profileView.StudentProfileId}' not exist, check again!"
                });
                profile.StudentProfileId = profileView.StudentProfileId;
            }

            var context = new ValidationContext(profile);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(profile, context, results);

            if (isValid)
            {
                _context.ParentProfiles.Update(profile);

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    return Conflict(new Response
                    {
                        Status = "Error",
                        Message = $"Some fields conflict with database, check again!"
                    });
                }

                return Ok(new {
                    Id = profile.Id,
                    UserId = profile.AccountId,
                    Name = profile.Name,
                    Address = profile.Address,
                    StudentProfileId = profile.StudentProfileId,
                    StudentProfile = new StudentProfileDTO(profile.StudentProfile)
                });
            }

            var errors = results.Select(item => item.ErrorMessage);
            return StatusCode(StatusCodes.Status406NotAcceptable, new Response
            {
                Status = "Error",
                Message = errors.FirstOrDefault()
            });
        }

        [HttpDelete("{parentProfileId}")]
        public async Task<IActionResult> Delete(int parentProfileId)
        {

            var profile = await _context.ParentProfiles.FindAsync(parentProfileId);
            if (profile == null)
            {
                return NotFound(new Response
                {
                    Status = "Error",
                    Message = $"Not found ParentProfile with Id='{parentProfileId}'"
                });
            }

            _context.ParentProfiles.Remove(profile);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch
            {
                return Conflict(new Response
                {
                    Status = "Error",
                    Message = "Can't delete profile because relationship data. Check again."
                });
            }

            return Ok(new Response
            {
                Status = "Success",
                Message = $"Deleted ParentProfile of {profile.Name}"
            });
        }

    }
}

