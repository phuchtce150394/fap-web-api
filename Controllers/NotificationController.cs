using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FapWebApi.Data;
using FapWebApi.Models;
using FapWebApi.Identity;
using FapWebApi.DTO;
using System.ComponentModel.DataAnnotations;
using FapWebApi.Tools;

namespace FapWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public NotificationController(ApplicationDbContext context)
        {
            _context = context;
        }


        [HttpGet]
        public async Task<IActionResult> GetAll(int pageIndex=1, int pageSize=2)
        {
            try {
                var notifications = _context.Notifications.OrderBy(item => item.Id);

                var paginatedItems = await PaginatedList<Notification>.CreateAsync(notifications.AsNoTracking(), pageIndex, pageSize);

                if(paginatedItems.TotalPages > 0 && paginatedItems.PageIndex > paginatedItems.TotalPages) return NotFound();

                return Ok(paginatedItems.Select(item => new NotificationDTO(item)));
            }
            catch {
                return StatusCode(500, new Response {
                    Status="Error",
                    Message="Server handle error, try again!"
                });
            }
            
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> Get(int Id) {

            var obj = await _context.Notifications.FindAsync(Id);

            if(obj == null) {
                return NotFound(new Response {
                    Status="Error",
                    Message=$"Not Found Notification of Id='{Id}'"
                });
            }

            return Ok( new NotificationDTO(obj));
        }


        [HttpPost]
        public async Task<IActionResult> Post(NotificationDTO dto)
        {

            Notification obj = new Notification(dto);

            var context = new ValidationContext(obj);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(obj, context, results);

            if (isValid)
            {
                
                _context.Notifications.Add(obj);

                try {
                    await _context.SaveChangesAsync();
                }
                catch {
                    return Conflict(new Response {
                        Status="Error",
                        Message=$"Some fields conflict with database, check again or contact us!"
                    });
                }

                return Ok(new NotificationDTO(obj));
                
            }

            var errors = results.Select(item => item.ErrorMessage);

            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });

        }

        [HttpPut("{Id}")]
        public async Task<IActionResult> Put(int Id, NotificationDTO dto)
        {

            Notification obj = await _context.Notifications.FindAsync(Id);

            if(obj == null) {
                return NotFound(new Response {
                    Status="Error",
                    Message=$"Not Found Notification of Id='{Id}'"
                });
            }

            if(dto.Title != null) obj.Title=dto.Title;
            if(dto.Content != null) obj.Content=dto.Content;

            var context = new ValidationContext(obj);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(obj, context, results);

            if (isValid)
            {
                
                _context.Notifications.Update(obj);

                try {
                    await _context.SaveChangesAsync();
                }
                catch {
                    return Conflict(new Response {
                        Status="Error",
                        Message=$"Some fields conflict with database, check again or contact us!"
                    });
                }

                return Ok(new NotificationDTO(obj));
                
            }

            var errors = results.Select(item => item.ErrorMessage);

            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });

        }
        
        [HttpDelete("{Id}")]
        public async Task<IActionResult> DeleteAsync(int Id)
        {
            Notification obj = await _context.Notifications.FindAsync(Id);

            if(obj == null) {
                return NotFound(new Response {
                    Status="Error",
                    Message=$"Not Found Notification of Id='{Id}'"
                });
            }

            try {
                _context.Notifications.Remove(obj);
                await _context.SaveChangesAsync();

                return Ok(new Response {
                    Status="Success",
                    Message=$"Notification '{obj.Title}' has been deleted."
                });
            }
            catch {
                return Conflict(new Response {
                    Status="Error",
                    Message="An error handle while processing data, try again or contact us!"
                });
            }
        }

        [HttpGet("Search")]
        public IActionResult Search(string keyword, int maxResult = 5)
        {

            if(keyword == null) return BadRequest(new Response{
                Status="Error",
                Message="Missing keyword!"
            });

            var items = _context.Notifications.Where(item => item.Title.Contains(keyword) || item.Content.Contains(keyword)).OrderBy(item => item.Id).Take(maxResult);

            return Ok(items.Select(obj => new NotificationDTO(obj)));

        }

    }
}