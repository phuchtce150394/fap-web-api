using System.Linq;
using System.Threading.Tasks;
using FapWebApi.Data;
using FapWebApi.DTO;
using FapWebApi.Tools;
using FapWebApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FapWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClassController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public ClassController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var classes = _context.Classes.Include(item => item.StudentProfiles).OrderBy(item => item.Name).Select(item => new ClassDTO(item));

            return Ok(classes);
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> Get(int Id)
        {
            var item = await _context.Classes.Include(item => item.StudentProfiles).Include(item => item.Semester).FirstOrDefaultAsync(item => item.Id == Id);

            if (item == null) return NotFound(new Response{
                Status = "Error",
                Message = "Class can not found!"
            });
            
            return Ok(new {
                Id=item.Id,
                Name=item.Name,
                SemesterId=item.SemesterId,
                Semester=item.Semester,
                StudentProfiles=item.StudentProfiles.Select(profile => new StudentProfileDTO(profile)) 
            });

        }

        [HttpPost]
        public async Task<IActionResult> Post(ClassDTO dto) {

            var checkNameDuplicate = await _context.Classes.AnyAsync(item => item.Name == dto.Name);
            if(checkNameDuplicate) return Conflict(new Response {
                Status="Error",
                Message=$"Name '{dto.Name}' already existed!"
            });

            var checkSemesterExist = await _context.Semesters.FindAsync(dto.SemesterId);
            if(checkSemesterExist == null) return Conflict(new Response {
                Status="Error",
                Message="SemesterId invalid!"
            });

            try {

                Class item = new Class() {
                    Id=0,
                    Name=dto.Name,
                    Status=dto.Status,
                    SemesterId=dto.SemesterId
                };
                _context.Classes.Add(item);
                
                await _context.SaveChangesAsync();
                return Ok(new {
                    Id=item.Id,
                    Name=item.Name,
                    SemesterId=item.SemesterId,
                    Semester=item.Semester,
                    StudentProfiles=item.StudentProfiles.Select(profile => new StudentProfileDTO(profile)) 
                });
            }
            catch {
                return Conflict(new Response {
                    Status="Error",
                    Message="An error handle while processing data, try again!"
                });
            }
        }
    
        [HttpGet("GetByClassName")]
        public async Task<IActionResult> GetByClassName(string className) {

            var item = await _context.Classes.Include(item => item.StudentProfiles).Include(item => item.Semester).FirstOrDefaultAsync(item => item.Name == className);

            if (item == null) return NotFound(new Response{
                Status = "Error",
                Message = "Class can not found!"
            });
            
            return Ok(new {
                Id=item.Id,
                Name=item.Name,
                SemesterId=item.SemesterId,
                Semester=item.Semester,
                StudentProfiles=item.StudentProfiles.Select(profile => new StudentProfileDTO(profile)) 
            });
        }
    
        [HttpPut("{Id}")]
        public async Task<IActionResult> Update(ClassDTO dto, int Id) {

            Class obj = await _context.Classes.FindAsync(Id);
            if(obj == null) return NotFound(new Response {
                Status="Error",
                Message="Class not found!"
            });

            var checkNameDuplicate = await _context.Classes.AnyAsync(item => !item.Name.Equals(obj.Name) && item.Name.Equals(dto.Name));
            if(checkNameDuplicate) return Conflict(new Response {
                Status="Error",
                Message=$"Name '{dto.Name}' already existed!"
            });

            var checkSemesterExist = await _context.Semesters.FindAsync(dto.SemesterId);
            if(checkSemesterExist == null) return Conflict(new Response {
                Status="Error",
                Message="SemesterId invalid!"
            });

            try {

                obj.Name = dto.Name;
                obj.Status = dto.Status;
                obj.SemesterId = dto.SemesterId;

                _context.Classes.Update(obj);
                await _context.SaveChangesAsync();
                return Ok(new ClassDTO(obj));
            }
            catch {
                return Conflict(new Response {
                    Status="Error",
                    Message="An error handle while processing data, try again!"
                });
            }

        }

        [HttpDelete("{Id}")]
        public async Task<IActionResult> Delete(int Id) {

            Class obj = await _context.Classes.Include(item => item.StudentProfiles).FirstOrDefaultAsync(item => item.Id == Id);
            if(obj == null) return NotFound(new Response {
                Status="Error",
                Message="Class not found!"
            });

            var checkActive = obj.StudentProfiles.Count > 0;
            if(checkActive) return Conflict(new Response {
                Status="Error",
                Message=$"Can't delete class '{obj.Name}' because conflict data, check again!"
            });

            try {
                _context.Classes.Remove(obj);
                await _context.SaveChangesAsync();
                return Ok(new Response {
                    Status="Success",
                    Message=$"Class '{obj.Name}' deleted."
                });
            }
            catch {
                return Conflict(new Response {
                    Status="Error",
                    Message=$"Can't delete class '{obj.Name}' because conflict data, check again!"
                });
            }

        }

        [HttpDelete("DeleteByClassName")]
        public async Task<IActionResult> DeleteByClassName(string className) {

            if(className == null) return BadRequest(new Response{
                Status="Waring",
                Message="Missing className!"
            });

            var obj = await _context.Classes.Include(item => item.StudentProfiles).Include(item => item.Semester).FirstOrDefaultAsync(item => item.Name == className);

            if (obj == null) return NotFound(new Response{
                Status = "Error",
                Message = "Class can not found!"
            });

            var checkActive = obj.StudentProfiles.Count > 0;
            if(checkActive) return Conflict(new Response {
                Status="Error",
                Message=$"Can't delete class '{obj.Name}' because conflict data, check again!"
            });

            try {
                _context.Classes.Remove(obj);
                await _context.SaveChangesAsync();
                return Ok(new Response {
                    Status="Success",
                    Message=$"Class '{obj.Name}' deleted."
                });
            }
            catch {
                return Conflict(new Response {
                    Status="Error",
                    Message=$"Can't delete class '{obj.Name}' because conflict data, check again!"
                });
            }

        }

        [HttpPut("AddStudent")]
        public async Task<IActionResult> AddStudent(string className, string studentRollNumber) {

            if(className == null) return BadRequest(new Response {
                Status="Warning",
                Message="Missing className!"
            });

            if(studentRollNumber == null) return BadRequest(new Response {
                Status="Warning",
                Message="Missing RollNumber!"
            });

            Class c = await _context.Classes.FirstOrDefaultAsync(item => item.Name == className);
            if(c == null) return BadRequest(new Response {
                Status="Error",
                Message=$"ClassName '{className}' not exist!"
            });

            StudentProfile student = await _context.StudentProfiles.Include(s => s.Classes).FirstOrDefaultAsync(item => item.RollNumber == studentRollNumber);
            if(student == null) return BadRequest(new Response {
                Status="Error",
                Message=$"Student with RollNumber '{studentRollNumber}' not exist!"
            });

            var checkIn = student.Classes.Any(item => item.Name == className);
            if(checkIn) return Ok(new Response {
                Status="Warning",
                Message=$"Student already in class '{className}'"
            });

            try {

                student.Classes.Add(c);
                _context.SaveChanges();
            }
            catch {
                return Conflict(new Response{
                    Status="Error",
                    Message="An error handled while processing data, try again or contact us!"
                });
            }

            return Ok(new Response {
                Status="Success",
                Message=$"Added Student '{studentRollNumber}' into class '{className}' success."
            });
        }

        [HttpPut("RemoveStudent")]
        public async Task<IActionResult> RemoveStudent(string className, string studentRollNumber) {

            if(className == null) return BadRequest(new Response {
                Status="Warning",
                Message="Missing className!"
            });

            if(studentRollNumber == null) return BadRequest(new Response {
                Status="Warning",
                Message="Missing RollNumber!"
            });

            Class c = await _context.Classes.FirstOrDefaultAsync(item => item.Name == className);
            if(c == null) return BadRequest(new Response {
                Status="Error",
                Message=$"ClassName '{className}' not exist!"
            });

            StudentProfile student = await _context.StudentProfiles.Include(s => s.Classes).FirstOrDefaultAsync(item => item.RollNumber == studentRollNumber);
            if(student == null) return BadRequest(new Response {
                Status="Error",
                Message=$"Student with RollNumber '{studentRollNumber}' not exist!"
            });

            var checkIn = student.Classes.Any(item => item.Name == className);
            if(!checkIn) return Ok(new Response {
                Status="Warning",
                Message=$"Student not in class '{className}'"
            });

            try {

                student.Classes.Remove(c);
                _context.SaveChanges();
            }
            catch {
                return Conflict(new Response{
                    Status="Error",
                    Message="An error handled while processing data, try again or contact us!"
                });
            }

            return Ok(new Response {
                Status="Success",
                Message=$"Remove Student '{studentRollNumber}' out class '{className}' success."
            });
        }
    }
}