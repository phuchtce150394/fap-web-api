using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using FapWebApi.Data;
using FapWebApi.DTO;
using FapWebApi.Identity;
using FapWebApi.Models;
using FapWebApi.Tools;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static Google.Apis.Auth.GoogleJsonWebSignature;

namespace FapWebApi.Controllers
{

    // [Authorize(Roles = UserRoles.Student)]
    [Route("api/[controller]")]
    [ApiController]
    public class StudentProfileController : ControllerBase
    {

        ApplicationDbContext _context;
        UserManager<Account> _userManager;

        public StudentProfileController(ApplicationDbContext context, UserManager<Account> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        
        [HttpGet]
        public async Task<IActionResult> GetAll(int pageIndex=1, int pageSize=2)
        {
            try {
                var students = _context.StudentProfiles.OrderBy(item => item.Name);

                var paginatedStudents = await PaginatedList<StudentProfile>.CreateAsync(students.AsNoTracking(), pageIndex, pageSize);

                if(paginatedStudents.TotalPages > 0 && paginatedStudents.PageIndex > paginatedStudents.TotalPages) return NotFound();

                return Ok(paginatedStudents.Select(profile => new StudentProfileDTO(profile)));
            }
            catch {
                return StatusCode(500, new Response {
                    Status="Error",
                    Message="Server handle error, try again!"
                });
            }
            
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> Get(int Id) {

            var profile = await _context.StudentProfiles.FindAsync(Id);

            if(profile == null) {
                return NotFound(new Response {
                    Status="Error",
                    Message=$"Not Found StudentProfile of Id='{Id}'"
                });
            }

            return Ok( new StudentProfileDTO(profile));
        }

        [HttpGet("GetByUserId")]
        public async Task<IActionResult> GetByUserId(string userId) {

            var profile = await _context.StudentProfiles.FirstOrDefaultAsync(profile => profile.AccountId == userId);

            if(profile == null) {
                return NotFound(new Response {
                    Status="Error",
                    Message=$"Not Found StudentProfile of UserId='{userId}'"
                });
            }

            return Ok( new StudentProfileDTO(profile));
        }

        [HttpGet("GetByRollNumber")]
        public async Task<IActionResult> GetByRollNumber(string rollNummber) {

            var profile = await _context.StudentProfiles.FirstOrDefaultAsync(profile => profile.RollNumber == rollNummber);

            if(profile == null) {
                return NotFound(new Response {
                    Status="Error",
                    Message=$"Not Found StudentProfile of rollNumber='{rollNummber}'"
                });
            }

            return Ok( new StudentProfileDTO(profile));
        }

        [HttpPut]
        public async Task<IActionResult> UpdateAsync(StudentProfileDTO dto)
        {

            StudentProfile profile = await _context.StudentProfiles.FindAsync(dto.Id);

            if(profile == null)
            {
                return NotFound(new Response {Status="Error", Message=$"No have any StudentProfile with Id '{dto.Id}'"});
            }

            if(dto.Name != null) profile.Name = dto.Name;
            if(dto.ImageURL != null) profile.ImageURL = dto.ImageURL;
            if(dto.Gender != null) profile.Gender = dto.Gender.Value;
            if(dto.Birthday != null) profile.Birthday = dto.Birthday;
            if(dto.RollNumber != null) {

                var checkRollNumberExist = await _context.StudentProfiles.AnyAsync(item => !item.RollNumber.Equals(profile.RollNumber) && item.RollNumber.Equals(dto.RollNumber));
                if(checkRollNumberExist) return Conflict(new Response {
                    Status="Error",
                    Message=$"RollNumber '{profile.RollNumber}' already existed!"
                });

                profile.RollNumber = dto.RollNumber;

            }
            if(dto.Address != null) profile.Address = dto.Address;
            if(dto.IdCard != null) profile.IdCard = dto.IdCard;
            if(dto.PhoneNumber != null) profile.PhoneNumber = dto.PhoneNumber;
            if(dto.MajorId != null) profile.MajorId = dto.MajorId;

            var context = new ValidationContext(profile);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(profile, context, results);

            if (isValid)
            {
                _context.StudentProfiles.Update(profile);
                try {
                    await _context.SaveChangesAsync();
                }
                catch {
                    return Conflict(new Response {
                        Status="Error",
                        Message=$"Some fields conflict with database, check again!"
                    });
                }

                return Ok(new StudentProfileDTO(profile));
            }

            var errors = results.Select(item => item.ErrorMessage);
            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });

        }

        [HttpDelete("{Id}")]
        public async Task<IActionResult> Delete(int Id) {

            var profile = await _context.StudentProfiles.FindAsync(Id);
            if(profile == null) {
                return NotFound(new Response {
                    Status="Error",
                    Message="Not found StudentProfile"
                });
            }

            _context.StudentProfiles.Remove(profile);
            try {
                await _context.SaveChangesAsync();
            }
            catch {
                return Conflict(new Response {
                    Status="Error",
                    Message="Can't delete profile because relationship data. Check again."
                });
            }

            return Ok(new Response {
                Status="Success",
                Message=$"Deleted StudentProfile of {profile.Name}"
            });
        }
        

        [HttpPost("UpdateImage")]
        public IActionResult UpdateImage(int studentProfileId, IFormFile image) 
        {


            return Ok();
        }

    }
}