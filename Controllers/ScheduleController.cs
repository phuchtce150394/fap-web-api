using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FapWebApi.Models;
using FapWebApi.Data;
using FapWebApi.DTO;
using System.ComponentModel.DataAnnotations;
using FapWebApi.Tools;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace FapWebApi.Controllers
{

    
    [Route("api/[controller]")]
    [ApiController]
    public class ScheduleController : ControllerBase
    {

        ApplicationDbContext _context;
        public ScheduleController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpPost("AddMultiple")]
        public async Task<IActionResult> AddMultipleAsync(PostMultipleScheduleDTO info)
        {
            
            var classItem = await _context.Classes.Include(item => item.StudentProfiles).FirstOrDefaultAsync(item => item.Name==info.ClassName);
            if(classItem == null) return Conflict(new Response {
                Status="Error",
                Message=$"ClassName '{info.ClassName}' invalid!"
            });

            var checkSubject = await _context.Subjects.FirstOrDefaultAsync(item => item.Code == info.SubjectCode);
            if(checkSubject == null) return Conflict(new Response {
                Status="Error",
                Message=$"SubjectCode '{info.SubjectCode}' invalid!"
            });

            var checkTeacher = await _context.TeacherProfiles.FirstOrDefaultAsync(item => item.RollNumber==info.TeacherCode);
            if(checkTeacher == null) return Conflict(new Response {
                Status="Error",
                Message=$"Teacher '{info.TeacherCode}' invalid!"
            });

            var checkRoom = await _context.Rooms.FirstOrDefaultAsync(item => item.RoomCode==info.RoomCode);
            if(checkRoom == null) return Conflict(new Response {
                Status="Error",
                Message=$"RoomCode '{info.RoomCode}' invalid!"
            });

            if(checkRoom.IsActive == false) return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=$"Room '{checkRoom.RoomCode}' no active!"
            });

            var checkSemester = await _context.Semesters.FirstOrDefaultAsync(item => item.Code == info.SemesterCode);
            if(checkSemester == null) return Conflict(new Response {
                Status="Error",
                Message=$"SemesterCode invalid!"
            });

            var checkStartDate = info.StartDate.Date >= checkSemester.StartDate.Date && info.StartDate.Date <= checkSemester.EndDate.Date;
            if(checkStartDate == false) return Conflict(new Response {
                Status="Error",
                Message=$"StartDate={info.StartDate.ToShortDateString()} while StartDate Of Semester is {checkSemester.StartDate.ToShortDateString()}, EndDate is {checkSemester.EndDate.ToShortDateString()}. Check again!"
            });

            List<object> responseItems = new List<object>();

            var slotOfWeek = info.CycleDetails;
            TimeSpan unitTime = new TimeSpan(1, 0, 0, 0); // 1 day
            DateTime StartDate = info.StartDate.Date;
            DateTime EndDate   = checkSemester.EndDate.Date;

            int slotsCounter = 0;
            for(DateTime date = StartDate; date < EndDate; date += unitTime) 
            {
                int dayOfWeek = (int) date.DayOfWeek;
                int[] slots = slotOfWeek[dayOfWeek];
                foreach (int SlotNo in slots)
                {
                    if(await RoomController.IsEmptyAsync(_context, info.RoomCode, date, SlotNo)) slotsCounter += 1;
                    else {
                        return BadRequest(new Response {
                            Status="Error",
                            Message="Some schedule can't created because there is a schedule existed in cycle."
                        });
                    }
                }
            }
            if(slotsCounter < info.NumberOfSlots) return BadRequest(new Response {
                Status="Error",
                Message=$"Duration from {StartDate.ToShortDateString()} to end of semester {info.SemesterCode} - {EndDate.ToShortDateString()} is not enough to create {info.NumberOfSlots} schedules! Check again!"
            });

            int successCounters = 0;
            
            for(DateTime date = StartDate; date < EndDate; date += unitTime)
            {
                int dayOfWeek = (int) date.DayOfWeek;
                int[] slots = slotOfWeek[dayOfWeek];
                Array.Sort(slots);
                foreach (int SlotNo in slots)
                {
                    if(successCounters >= info.NumberOfSlots) break;

                    var checkSlot = SlotNo <= 8 && SlotNo > 0;
                    if(checkSlot == false) 
                    {
                        responseItems.Add(new {
                            Status="Error",
                            Item=$"Schedule at {date.ToShortDateString()}, slot {SlotNo} can't process!",
                            Detail="SlotNo only accept 1-8!"
                        });

                        continue;
                    }

                    var checkRoomScheduleDuplicate = await _context.Schedules.FirstOrDefaultAsync(item => item.Date.Date.Equals(date) && item.RoomCode==info.RoomCode 
                    && item.SlotNo==SlotNo);
                    if(checkRoomScheduleDuplicate != null) 
                    {
                        responseItems.Add(new {
                            Status="Error",
                            Item=$"Schedule at {date.ToShortDateString()}, slot {SlotNo} can't process!",
                            Detail=$"Schedule at {info.RoomCode}, slot {SlotNo}, date {date.ToShortDateString()} already existed! Check again!"
                        });

                        continue;
                    }

                    Schedule checkClassOrTeacherDuplicate = await _context.Schedules.FirstOrDefaultAsync(item => (item.ClassName==info.ClassName || item.TeacherCode == info.TeacherCode) 
                    && item.Date==date && item.SlotNo==SlotNo);
                    if(checkClassOrTeacherDuplicate != null) {
                        responseItems.Add(new {
                            Status="Error",
                            Item=$"Schedule at {date.ToShortDateString()}, slot {SlotNo} can't process!",
                            Detail=$"Teacher or Class can't join many locations at the same time! Check ScheduleId='{checkClassOrTeacherDuplicate.Id}'!"
                        });
                        continue;
                    }

                    Schedule schedule = new Schedule(){
                        ClassName=classItem.Name,
                        SubjectCode=checkSubject.Code,
                        TeacherCode=checkTeacher.RollNumber,
                        RoomCode=checkRoom.RoomCode,
                        Date=date,
                        SlotNo=SlotNo
                    };

                    _context.Schedules.Add(schedule);
                    try {
                        await _context.SaveChangesAsync();

                        responseItems.Add(new {
                            Status="Success",
                            Item=$"Schedule at {date.ToShortDateString()}, slot {SlotNo} create success!",
                            Detail= new ScheduleDTO(schedule)
                        });

                        successCounters += 1;
                    }
                    catch {
                        responseItems.Add(new {
                            Status="Error",
                            Item=$"Schedule at {date.ToShortDateString()}, slot {SlotNo} can't process!",
                            Detail=$"Some errors handle while create this object. Try again later!"
                        });
                    }

                }
            }


            return Ok(new {
                numberOfScheduleCreated=successCounters,
                sumaries=$"System created {successCounters} schedule from {StartDate.ToShortDateString()} to {EndDate.ToShortDateString()} of Semester {info.SemesterCode} for class {info.ClassName}.",
                details=responseItems,
            });
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync(ScheduleDTO dto)
        {
            Schedule schedule = new Schedule(dto);

            var context = new ValidationContext(schedule);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(schedule, context, results);

            if (isValid)
            {
                
                var checkClassName = await _context.Classes.FirstOrDefaultAsync(item => item.Name == schedule.ClassName);
                if(checkClassName == null) return Conflict(new Response {
                    Status="Error",
                    Message=$"ClassName '{schedule.ClassName}' invalid!"
                });

                var checkSubject = await _context.Subjects.FirstOrDefaultAsync(item => item.Code == schedule.SubjectCode);
                if(checkSubject == null) return Conflict(new Response {
                    Status="Error",
                    Message=$"SubjectCode '{schedule.SubjectCode}' invalid!"
                });

                var checkTeacher = await _context.TeacherProfiles.FirstOrDefaultAsync(item => item.RollNumber==schedule.TeacherCode);
                if(checkTeacher == null) return Conflict(new Response {
                    Status="Error",
                    Message=$"Teacher '{schedule.TeacherCode}' invalid!"
                });

                var checkSlot = schedule.SlotNo <= 8 && schedule.SlotNo > 0;
                if(checkSlot == false) return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                    Status="Error",
                    Message="SlotNo only accept 1-8!"
                });

                var checkRoom = await _context.Rooms.FirstOrDefaultAsync(item => item.RoomCode==schedule.RoomCode);
                if(checkRoom == null) return Conflict(new Response {
                    Status="Error",
                    Message=$"RoomCode '{schedule.RoomCode}' invalid!"
                });

                if(checkRoom.IsActive == false) return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                    Status="Error",
                    Message=$"Room '{checkRoom.RoomCode}' no active!"
                });

                var checkRoomScheduleDuplicate = await _context.Schedules.FirstOrDefaultAsync(item => item.Date.Date.Equals(schedule.Date.Date) && item.RoomCode==schedule.RoomCode && item.SlotNo==schedule.SlotNo);
                if(checkRoomScheduleDuplicate != null) return Conflict(new Response {
                    Status="Error",
                    Message=$"Schedule at {schedule.RoomCode}, slot {schedule.SlotNo}, date {schedule.Date.Date.ToShortDateString()} already existed! Check again!"
                });
                

                _context.Schedules.Add(schedule);

                try {
                    await _context.SaveChangesAsync();
                }
                catch {

                    Schedule conflictSchedule = await _context.Schedules.FirstOrDefaultAsync(item => (item.ClassName==schedule.ClassName || item.TeacherCode == schedule.TeacherCode) && item.Date==schedule.Date && item.SlotNo==schedule.SlotNo);

                    return Conflict(new {
                        Status="Error",
                        Message=$"Teacher or Class can't join many locations at the same time! Check ScheduleId='{conflictSchedule.Id}'!",
                        data=new ScheduleDTO(conflictSchedule)
                    });
                }

                return Ok(new ScheduleDTO(schedule));
                
            }

            var errors = results.Select(item => item.ErrorMessage);

            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });

        }

        [HttpGet("GetByClass")]
        public async Task<IActionResult> GetByClassAsync(string ClassName, DateTime StartDate, DateTime EndDate)
        {

    
            var checkClass = await _context.Classes.FirstOrDefaultAsync(item => item.Name == ClassName);
            if(checkClass == null) return NotFound(new Response{
                Status="Error",
                Message=$"ClassName '{ClassName}' invalid!"
            });

            var schedules = await _context.Schedules.Where(item => item.ClassName == ClassName)
                            .Where(item => (StartDate == DateTime.MinValue || StartDate.Date <= item.Date.Date) && ( EndDate == DateTime.MinValue || item.Date.Date <= EndDate.Date))
                            .ToListAsync();
            

            return Ok(schedules.Select(obj => new ScheduleDTO(obj)).OrderBy(obj => obj.Date).ThenBy(obj => obj.SlotNo));
        }

        [HttpGet("GetByStudent")]
        public async Task<IActionResult> GetByStudentAsync(string StudentCode, DateTime StartDate, DateTime EndDate)
        {

            var checkStudent = await _context.StudentProfiles.Include(item => item.Classes).FirstOrDefaultAsync(item => item.RollNumber==StudentCode);
            if(checkStudent == null) return NotFound(new Response{
                Status="Error",
                Message=$"StudentCode {StudentCode} invalid!"
            });

            var classesOfStudent  = checkStudent.Classes.Select(item => item.Name).ToList();

            var coursesOfStudent = await _context.RegisterCourses.Where(item => item.StudentRollNumber==StudentCode)
                                    .ToListAsync();
            
            var subjectsOfStudent = new List<string>();
            foreach(var course in coursesOfStudent) 
            {
                Semester semester = await _context.Semesters.FirstOrDefaultAsync(item => item.Code==course.SemesterCode);
                if(StartDate != DateTime.MinValue && semester.EndDate.Date <= StartDate.Date) continue;
                if(EndDate != DateTime.MinValue && EndDate.Date <= semester.StartDate.Date) continue;
                if(course.isPayment == false) continue;

                subjectsOfStudent.Add(course.SubjectCode);
            }

            var schedulesOfStudent = await _context.Schedules.Where(item => classesOfStudent.Contains(item.ClassName))
                                    .Where(item => subjectsOfStudent.Contains(item.SubjectCode))
                                    .Where(item => (StartDate == DateTime.MinValue || StartDate.Date <= item.Date.Date) && ( EndDate == DateTime.MinValue || item.Date.Date <= EndDate.Date))
                                    .ToListAsync();

            return Ok(schedulesOfStudent.Select(item => new ScheduleDTO(item)).OrderBy(obj => obj.Date).ThenBy(obj => obj.SlotNo));
        }

        [HttpGet("GetByTeacher")]
        public async Task<IActionResult> GetByTeacherAsync(string TeacherCode, DateTime StartDate, DateTime EndDate)
        {

            var checkTeacher = await _context.TeacherProfiles.FirstOrDefaultAsync(item => item.RollNumber==TeacherCode);
            if(checkTeacher==null) return NotFound(new Response{
                Status="Error",
                Message=$"TeacherCode {TeacherCode} invalid!"
            });

            var schedulesOfTeacher = await _context.Schedules.Where(item => item.TeacherCode==TeacherCode)
            .Where(item => (StartDate == DateTime.MinValue || StartDate.Date <= item.Date.Date) && ( EndDate == DateTime.MinValue || item.Date.Date <= EndDate.Date))
            .ToListAsync();

            return Ok(schedulesOfTeacher.Select(item => new ScheduleDTO(item)).OrderBy(obj => obj.Date).ThenBy(obj => obj.SlotNo));
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> Get(int Id)
        {
            var schedule = await _context.Schedules.FindAsync(Id);
            if(schedule==null) return NotFound(new Response{
                Status="Error",
                Message=$"Schedule with Id='{Id}' not exist!"
            });


            
            return Ok(new ScheduleDTO(schedule));
        }

        [HttpPut("{Id}")]
        public async Task<IActionResult> PutAsync(int Id, ScheduleDTO dto) 
        {
            var checkSchedule = await _context.Schedules.FindAsync(Id);
            if(checkSchedule==null) return NotFound(new Response{
                Status="Error",
                Message=$"Schedule with Id='{Id}' not found!"
            });

            Schedule schedule = new Schedule(dto);

            var context = new ValidationContext(schedule);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(schedule, context, results);

            if (isValid)
            {
                
                var checkClassName = await _context.Classes.FirstOrDefaultAsync(item => item.Name == schedule.ClassName);
                if(checkClassName == null) return Conflict(new Response {
                    Status="Error",
                    Message=$"ClassName '{schedule.ClassName}' invalid!"
                });

                var checkSubject = await _context.Subjects.FirstOrDefaultAsync(item => item.Code == schedule.SubjectCode);
                if(checkSubject == null) return Conflict(new Response {
                    Status="Error",
                    Message=$"SubjectCode '{schedule.SubjectCode}' invalid!"
                });

                var checkTeacher = await _context.TeacherProfiles.FirstOrDefaultAsync(item => item.RollNumber==schedule.TeacherCode);
                if(checkTeacher == null) return Conflict(new Response {
                    Status="Error",
                    Message=$"Teacher '{schedule.TeacherCode}' invalid!"
                });

                var checkSlot = schedule.SlotNo <= 8 && schedule.SlotNo > 0;
                if(checkSlot == false) return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                    Status="Error",
                    Message="SlotNo only accept 1-8!"
                });

                var checkRoom = await _context.Rooms.FirstOrDefaultAsync(item => item.RoomCode==schedule.RoomCode);
                if(checkRoom == null) return Conflict(new Response {
                    Status="Error",
                    Message=$"RoomCode '{schedule.RoomCode}' invalid!"
                });

                if(checkRoom.IsActive == false) return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                    Status="Error",
                    Message=$"Room '{checkRoom.RoomCode}' no active!"
                });

                var checkRoomScheduleDuplicate = await _context.Schedules.FirstOrDefaultAsync(item => item.Date.Date.Equals(schedule.Date.Date) && item.RoomCode==schedule.RoomCode && item.SlotNo==schedule.SlotNo);
                if(checkRoomScheduleDuplicate != null) return Conflict(new Response {
                    Status="Error",
                    Message=$"Schedule at {schedule.RoomCode}, slot {schedule.SlotNo}, date {schedule.Date.Date.ToShortDateString()} already existed! Check again!"
                });
                
                checkSchedule.ClassName   = schedule.ClassName;
                checkSchedule.Date        = schedule.Date;
                checkSchedule.SubjectCode = schedule.SubjectCode;
                checkSchedule.RoomCode    = schedule.RoomCode;
                checkSchedule.TeacherCode = schedule.TeacherCode;
                checkSchedule.SlotNo      = schedule.SlotNo;
                _context.Schedules.Update(checkSchedule);

                try {
                    await _context.SaveChangesAsync();
                }
                catch {

                    Schedule conflictSchedule = await _context.Schedules.FirstOrDefaultAsync(item => (item.ClassName==schedule.ClassName || item.TeacherCode == schedule.TeacherCode) && item.Date==schedule.Date && item.SlotNo==schedule.SlotNo);

                    return Conflict(new {
                        Status="Error",
                        Message=$"Some fields conflict with database, check again or contact us!",
                        data=new ScheduleDTO(conflictSchedule)
                    });
                }

                return Ok(new ScheduleDTO(schedule));
                
            }

            var errors = results.Select(item => item.ErrorMessage);

            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });
            
        }

        [HttpDelete("{Id}")]
        public async Task<IActionResult> DeleteAsync(int Id)
        {
            var checkSchedule = await _context.Schedules.FindAsync(Id);

            if(checkSchedule==null) return NotFound(new Response{
                Status="Error",
                Message=$"Schedule with Id='{Id}' not found!"
            });

            _context.Schedules.Remove(checkSchedule);
            try {
                await _context.SaveChangesAsync();
            }
            catch {
                return Conflict(new Response{
                    Status="Error",
                    Message="Can't delete this object, conflict data, check again!"
                });
            }

            return Ok(new Response{
                Status="Success",
                Message=$"Deleted schedule with Id='{Id}' success."
            });
        }
    }
}