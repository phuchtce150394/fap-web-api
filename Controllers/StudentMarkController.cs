
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FapWebApi.Models;
using FapWebApi.Data;
using FapWebApi.DTO;
using FapWebApi.Tools;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using Newtonsoft.Json.Linq;

namespace FapWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentMarkController : ControllerBase
    {
        ApplicationDbContext _context;

        public StudentMarkController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Get(string StudentCode, string SemesterCode, string SubjectCode)
        {
            
            var course = await _context.RegisterCourses.FirstOrDefaultAsync(item => item.StudentRollNumber==StudentCode && item.SemesterCode==SemesterCode && item.SubjectCode==SubjectCode);
            if(course==null) return NotFound(new Response{
                Status="Error",
                Message="Can't find any course."
            });

            if(course.isPayment == false) return StatusCode(StatusCodes.Status402PaymentRequired, new {
                Status="Error",
                Message="This course is unpaid!",
                data= new RegisterCourseDTO(course)
            });
            

            var marks = _context.StudentMarks.Include(item => item.Course).Where(item => item.RegisterCourseId==course.Id).AsEnumerable()
            .GroupBy(item => item.GradeCategory).ToDictionary(item => item.Key);
            
            return Ok(marks.Select(item => new { GradeCategory=item.Key, Items=item.Value.Select(mark => new StudentMarkDTO(mark))}));
        }

        [HttpGet("GetByCourseId/{CourseId}")]
        public async Task<IActionResult> Get(int CourseId)
        {
            
            var course = await _context.RegisterCourses.FindAsync(CourseId);
            if(course==null) return NotFound(new Response{
                Status="Error",
                Message=$"Can't find the course with Id='{CourseId}'."
            });

            if(course.isPayment == false) return StatusCode(StatusCodes.Status402PaymentRequired, new {
                Status="Error",
                Message="This course is unpaid!",
                data= new RegisterCourseDTO(course)
            });
            

            var marks = _context.StudentMarks.Include(item => item.Course).Where(item => item.RegisterCourseId==course.Id).AsEnumerable()
            .GroupBy(item => item.GradeCategory).ToDictionary(item => item.Key);
            
            return Ok(marks.Select(item => new { GradeCategory=item.Key, Items=item.Value.Select(mark => new StudentMarkDTO(mark))}));
        }

        [HttpPost]
        public async Task<IActionResult> Post(StudentMarkDTO dto)
        {
            
            StudentMark mark = new StudentMark(dto);

            var context = new ValidationContext(mark);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(mark, context, results);

            if (isValid)
            {
                
                var checkCourse = await _context.RegisterCourses.FindAsync(mark.RegisterCourseId);
                if(checkCourse==null) return Conflict(new Response{
                    Status="Error",
                    Message=$"Course with Id='{mark.RegisterCourseId}' not exist!"
                });

                _context.Add(mark);

                try {
                    await _context.SaveChangesAsync();
                }
                catch {

                    var conflictItem = await _context.StudentMarks.Include(item => item.Course)
                    .FirstOrDefaultAsync(item => item.RegisterCourseId==mark.RegisterCourseId && item.GradeCategory==mark.GradeCategory && item.GradeItem==mark.GradeItem);

                    return Conflict(new {
                        Status="Error",
                        Message=$"Some fields unique existed in database, check again or contact us!",
                        data= new StudentMarkDTO(conflictItem)
                    });
                }
                mark.Course = await _context.RegisterCourses.FindAsync(mark.RegisterCourseId);
                return Ok(new StudentMarkDTO(mark));
                
            }

            var errors = results.Select(item => item.ErrorMessage);

            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });
        }

        [HttpPut("{Id}")]
        public async Task<IActionResult> UpdateAsync(int Id, StudentMarkDTO dto)
        {

            var checkMark = await _context.StudentMarks.FindAsync(Id);
            if(checkMark==null) return NotFound(new Response{
                Status="Error",
                Message=$"Object with Id='{Id}' invalid!"
            });

            var mark = new StudentMark(dto);

            var context = new ValidationContext(mark);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(mark, context, results);

            if (isValid)
            {
                var checkCourse = await _context.RegisterCourses.FindAsync(mark.RegisterCourseId);
                if(checkCourse==null) return Conflict(new Response{
                    Status="Error",
                    Message=$"Course with Id='{mark.RegisterCourseId}' not exist!"
                });
                
                checkMark.RegisterCourseId = mark.RegisterCourseId;
                checkMark.Course = checkCourse;
                checkMark.GradeCategory = mark.GradeCategory;
                checkMark.GradeItem = mark.GradeItem;
                checkMark.Weight = mark.Weight;
                checkMark.Value = mark.Value;
                checkMark.Comment = mark.Comment;

                _context.StudentMarks.Update(checkMark);

                try {
                    await _context.SaveChangesAsync();
                }
                catch {

                    var conflictItem = await _context.StudentMarks.Include(item => item.Course)
                    .FirstOrDefaultAsync(item => item.RegisterCourseId==mark.RegisterCourseId && item.GradeCategory==mark.GradeCategory && item.GradeItem==mark.GradeItem);

                    return Conflict(new {
                        Status="Error",
                        Message=$"Some fields unique existed in database, check again or contact us!",
                        data= new StudentMarkDTO(conflictItem)
                    });
                }
                mark.Course = await _context.RegisterCourses.FindAsync(mark.RegisterCourseId);
                return Ok(new StudentMarkDTO(mark));
                
            }

            var errors = results.Select(item => item.ErrorMessage);

            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });

        }

        [HttpDelete("{Id}")]
        public async Task<IActionResult> DeleteAsync(int Id)
        {
            var mark = await _context.StudentMarks.FindAsync(Id);
            if(mark==null) return NotFound(new Response{
                Status="Error",
                Message=$"Mark with Id='{Id}' not existed!"
            });

            _context.StudentMarks.Remove(mark);
            try {
                await _context.SaveChangesAsync();
            }
            catch {

                return Conflict(new Response{
                    Status="Error",
                    Message="Can't deleted this object because relational data. Check again or contact us!"
                });
            }

            mark.Course = await _context.RegisterCourses.FindAsync(mark.RegisterCourseId);
            return Ok(new {
                Status="Success",
                Message="Deleted success.",
                data= new StudentMarkDTO(mark)
            });
        }
        
        [HttpGet("GetAvgMarkOfCourse")]
        public async Task<IActionResult> GetAvgMarkOfCourseAsync(int CourseId) 
        {

            var course = await _context.RegisterCourses.FindAsync(CourseId);
            if(course==null) return NotFound(new Response{
                Status="Error",
                Message=$"Can't find the course with Id='{CourseId}'."
            });

            if(course.isPayment == false) return StatusCode(StatusCodes.Status402PaymentRequired, new {
                Status="Error",
                Message="This course is unpaid!",
                data= new RegisterCourseDTO(course)
            });

            var marks = _context.StudentMarks.Include(item => item.Course).Where(item => item.RegisterCourseId==course.Id).ToList();
            double total = 0;
            foreach(StudentMark mark in marks)
            {
                total += (mark.Value * mark.Weight);
            }

            return Ok(new {
                course = new RegisterCourseDTO(course),
                average = total,
                status= total < 5 ? "Not Pass" : "Passed"
            });

        }
    }
}