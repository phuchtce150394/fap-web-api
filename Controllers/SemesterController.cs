using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using FapWebApi.Data;
using FapWebApi.DTO;
using FapWebApi.Tools;
using FapWebApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FapWebApi.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class SemesterController : ControllerBase{
        ApplicationDbContext _context;
        public SemesterController(ApplicationDbContext context)
        {
            _context = context;
        }
        

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var items = await _context.Semesters.Select(item => new SemesterDTO(item)).ToListAsync();
            return Ok(items);
        }


        [HttpGet("{Id}")]
        public async Task<IActionResult> Get(int Id)
        {
            var semester = await _context.Semesters.FindAsync(Id);

            if (semester == null) return NotFound(new Response{
                Status="Erorr",
                Message=$"Semester with Id='{Id}' not found!"
            });
            

            return Ok(new SemesterDTO(semester));
        }
        


        [HttpPut("{Id}")]
        public async Task<IActionResult> Put(int Id, SemesterDTO dto)
        {
            
            Semester semester = await _context.Semesters.FindAsync(Id);
            if (semester == null) return NotFound(new Response{
                Status="Erorr",
                Message=$"Semester with Id='{Id}' not found!"
            });

            var checkCode = await _context.Semesters.AnyAsync(item => !item.Code.Equals(semester.Code) && item.Code == dto.Code);
            if(checkCode) return Conflict(new Response{
                Status="Error",
                Message=$"SemesterCode '{dto.Code}' already existed!"
            });

            semester.Code=dto.Code;
            semester.Name=dto.Name;
            semester.StartDate=dto.StartDate;
            semester.EndDate=dto.EndDate;

            var context = new ValidationContext(semester);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(semester, context, results);

            if (isValid)
            {
                
                _context.Semesters.Update(semester);

                try {
                    await _context.SaveChangesAsync();
                }
                catch {
                    return Conflict(new Response {
                        Status="Error",
                        Message=$"Some fields conflict with database, check again or contact us!"
                    });
                }

                return Ok(new SemesterDTO(semester));
                
            }

            var errors = results.Select(item => item.ErrorMessage);

            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });
            
        }

        
        [HttpPost]
        public async Task<IActionResult> Post(SemesterDTO dto)
        {

            Semester semester = new Semester(dto);

            var checkCode = await _context.Semesters.AnyAsync(item => item.Code == dto.Code);
            if(checkCode) return Conflict(new Response{
                Status="Error",
                Message=$"SemesterCode '{dto.Code}' already existed!"
            });

            var context = new ValidationContext(semester);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(semester, context, results);

            if (isValid)
            {
                
                _context.Semesters.Add(semester);

                try {
                    await _context.SaveChangesAsync();
                }
                catch {
                    return Conflict(new Response {
                        Status="Error",
                        Message=$"Some fields conflict with database, check again or contact us!"
                    });
                }

                return Ok(new SemesterDTO(semester));
                
            }

            var errors = results.Select(item => item.ErrorMessage);

            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });
        }

        
        [HttpDelete("{Id}")]
        public async Task<IActionResult> Delete(int Id)
        {
            Semester semester = await _context.Semesters.FindAsync(Id);
            if (semester == null) return NotFound(new Response{
                Status="Erorr",
                Message=$"Semester with Id='{Id}' not found!"
            });

            try {

                _context.Semesters.Remove(semester);
                await _context.SaveChangesAsync();

                return Ok(new Response {
                    Status="Success",
                    Message=$"Semester '{semester.Code}' has been deleted."
                });
            }
            catch {
                return Conflict(new Response {
                    Status="Error",
                    Message="An error handle while processing data, try again or contact us!"
                });
            }

        }

    }
}