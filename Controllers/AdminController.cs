using FapWebApi.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using FapWebApi.Models;
using Microsoft.AspNetCore.Authorization;
using FapWebApi.Identity;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System.Linq;
using System;
using FapWebApi.Tools;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using FapWebApi.DTO;
using Microsoft.EntityFrameworkCore;

namespace FapWebApi.Controllers
{
    // [Authorize(Roles = UserRoles.Admin)]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        ApplicationDbContext _context;
        UserManager<Account> _userManager;
        RoleManager<IdentityRole> _roleManager;

        public AdminController(ApplicationDbContext context, UserManager<Account> userManager, RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        [HttpGet("GetAllRoles")]
        public IActionResult GetAllRoles()
        {

            return Ok(_roleManager.Roles.Select(item => item.Name));
        }
        
        [HttpPost("CreateParentProfile")]
        public async Task<IActionResult> CreateParentProfile(ParentProfileDTO profile)
        {

            Account user = await _userManager.FindByIdAsync(profile.UserId);
            if(user == null) {
                return NotFound(new Response {
                    Status="Error",
                    Message=$"No user with Id='{profile.UserId}'"
                });
            }

            var userRoles = await _userManager.GetRolesAsync(user);
            if(!userRoles.Contains(UserRoles.Parent)) {
                return Unauthorized(new Response {
                    Status="Error",
                    Message=$"User '{user.UserName}' don't have parent role"
                });
            }

            ParentProfile parentProfile = _context.ParentProfiles.FirstOrDefault(item => item.AccountId == user.Id);
            if(parentProfile != null) {
                return Conflict(new Response {
                    Status="Error",
                    Message=$"User with id='{user.Id}' already has ParentProfile!"
                });
            }

            
            var student = await _context.StudentProfiles.FindAsync(profile.StudentProfileId);
            if(student == null) return Conflict(new Response {
                Status = "Error",
                Message = $"Student with id='{profile.StudentProfileId}' not exist, check again!"
            });
            
            parentProfile = new ParentProfile() {
                AccountId=profile.UserId,
                Name = profile.Name,
                Address = profile.Address,
                StudentProfileId=profile.StudentProfileId
            };

            var context = new ValidationContext(parentProfile);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(parentProfile, context, results);

            if (isValid)
            {
                await _context.ParentProfiles.AddAsync(parentProfile);
                try {

                    await _context.SaveChangesAsync();
                }
                catch {
                    return Conflict(new Response {
                        Status="Error",
                        Message="Some data field is invalid!"
                    });
                }
                return Ok( new {
                    Id = parentProfile.Id,
                    UserId = parentProfile.AccountId,
                    Name = parentProfile.Name,
                    Address = parentProfile.Address,
                    StudentProfileId = parentProfile.StudentProfileId,
                    StudentProfile = new StudentProfileDTO(parentProfile.StudentProfile)
                });
            }

            foreach (var validationResult in results)
            {
                //validation errors
            }

            var errors = results.Select(item => item.ErrorMessage);
            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });

        }

        [HttpPost]
        [Route("CreateStudentProfile")]
        public async Task<IActionResult> CreateStudentProfile(StudentProfileDTO profile)
        {

            Account user = await _userManager.FindByIdAsync(profile.UserId);
            if (user == null)
            {
                return NotFound(new Response
                {
                    Status = "Error",
                    Message = $"No user with Id='{profile.UserId}'"
                });
            }

            var userRoles = await _userManager.GetRolesAsync(user);
            if (!userRoles.Contains(UserRoles.Student))
            {
                return Unauthorized(new Response
                {
                    Status = "Error",
                    Message = $"User '{user.UserName}' don't have '{UserRoles.Student}' role"
                });
            }

            StudentProfile studentProfile = _context.StudentProfiles.FirstOrDefault(item => item.AccountId == user.Id);
            if (studentProfile != null)
            {
                return Conflict(new Response
                {
                    Status = "Error",
                    Message = "This user already has StudentProfile."
                });
            }

            var checkRollNumberExist = await _context.StudentProfiles.AnyAsync(item => item.RollNumber == profile.RollNumber);
            if(checkRollNumberExist) return Conflict(new Response {
                Status="Error",
                Message=$"RollNumber '{profile.RollNumber}' already existed!"
            });

            studentProfile = new StudentProfile(profile);

            var context = new ValidationContext(studentProfile);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(studentProfile, context, results);

            if (isValid)
            {
                await _context.StudentProfiles.AddAsync(studentProfile);
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    return Conflict(new Response
                    {
                        Status = "Error",
                        Message = "Some fields conflict with database, check again!"
                    });
                }

                return Ok(new StudentProfileDTO(studentProfile));
            }

            var errors = results.Select(item => item.ErrorMessage);
            return StatusCode(StatusCodes.Status406NotAcceptable, new Response
            {
                Status = "Error",
                Message = errors.FirstOrDefault()
            });

        }

        [HttpPost]
        [Route("CreateTeacherProfile")]
        public async Task<IActionResult> CreateTeacherProfile(TeacherProfileDTO dto)
        {
            Account user = await _userManager.FindByIdAsync(dto.UserId);
            if (user == null)
            {
                return NotFound(new Response
                {
                    Status = "Error",
                    Message = $"No user with Id='{dto.UserId}'"
                });
            }

            var userRoles = await _userManager.GetRolesAsync(user);
            if (!userRoles.Contains(UserRoles.Teacher))
            {
                return Unauthorized(new Response
                {
                    Status = "Error",
                    Message = $"User '{user.UserName}' don't have '{UserRoles.Teacher}' role"
                });
            }

            var profile = _context.TeacherProfiles.FirstOrDefault(t => t.AccountId == user.Id);
            if (profile != null)
            {
                return Conflict(new Response
                {
                    Status = "Error",
                    Message = "This user already has TeacherProfile."
                });
            }

            profile = new TeacherProfile(dto);

            var context = new ValidationContext(profile);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(profile, context, results);

            if (isValid)
            {
                await _context.TeacherProfiles.AddAsync(profile);
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    return StatusCode(StatusCodes.Status406NotAcceptable, new Response
                    {
                        Status = "Error",
                        Message = "Some fields conflict with database, check again!"
                    });
                }

                return Ok(new TeacherProfileDTO(profile));
            }

            var errors = results.Select(item => item.ErrorMessage);
            return StatusCode(StatusCodes.Status406NotAcceptable, new Response
            {
                Status = "Error",
                Message = errors.FirstOrDefault()
            });

        }
        

        [HttpPost]
        [Route("CreateAdminProfile")]
        public async Task<IActionResult> CreateAdminProfileAsync(AdminProfileDTO profile)
        {

            Account user = await _userManager.FindByIdAsync(profile.UserId);
            if (user == null)
            {
                return NotFound(new Response
                {
                    Status = "Error",
                    Message = $"No user with Id='{profile.UserId}'"
                });
            }

            var userRoles = await _userManager.GetRolesAsync(user);
            if (!userRoles.Contains(UserRoles.Admin))
            {
                return Unauthorized(new Response
                {
                    Status = "Error",
                    Message = $"User '{user.UserName}' don't have admin role"
                });
            }

            AdminProfile adminProfile = _context.AdminProfiles.FirstOrDefault(item => item.AccountId == user.Id);
            if (adminProfile != null)
            {
                return Conflict(new Response
                {
                    Status = "Error",
                    Message = $"User with id='{user.Id}' already has AdminProfile!"
                });
            }

            adminProfile = new AdminProfile(profile);

            var context = new ValidationContext(adminProfile);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(adminProfile, context, results);

            if (isValid)
            {
                await _context.AdminProfiles.AddAsync(adminProfile);
                try {

                    await _context.SaveChangesAsync();
                }
                catch {
                    return Conflict(new Response {
                        Status="Error",
                        Message="Some data field is invalid!"
                    });
                }
                return Ok(new AdminProfileDTO(adminProfile));
            }

            var errors = results.Select(item => item.ErrorMessage);
            return StatusCode(StatusCodes.Status406NotAcceptable, new Response
            {
                Status = "Error",
                Message = errors.FirstOrDefault()
            });
        }


    }
}