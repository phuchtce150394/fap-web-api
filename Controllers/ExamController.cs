using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FapWebApi.Models;
using FapWebApi.Data;
using FapWebApi.DTO;
using System.ComponentModel.DataAnnotations;
using FapWebApi.Tools;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace FapWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExamController : ControllerBase
    {
        ApplicationDbContext _context;
        public ExamController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync(ExamDTO dto)
        {
            Exam exam = new Exam(dto);

            var context = new ValidationContext(exam);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(exam, context, results);

            if (isValid)
            {
                var checkCourse = await _context.RegisterCourses.FindAsync(exam.RegisterCourseId);
                if(checkCourse == null) return Conflict(new Response{
                    Status="Error",
                    Message=$"The course with Id='{exam.RegisterCourseId}' not exist!"
                });

                if(checkCourse.isPayment == false) return StatusCode(StatusCodes.Status406NotAcceptable, new {
                    Status="Waring",
                    Message=$"This course is unpaid!",
                    data=new RegisterCourseDTO(checkCourse)
                });
                
                var checkRoom = await _context.Rooms.FirstOrDefaultAsync(item => item.RoomCode==exam.RoomCode);
                if(checkRoom==null) return Conflict(new Response{
                    Status="Error",
                    Message=$"Room with Code='{exam.RoomCode}' not exist!"
                });

                if(checkRoom.IsActive == false) return StatusCode(StatusCodes.Status406NotAcceptable, new Response{
                    Status="Waring",
                    Message=$"The room {checkRoom.RoomCode} is not active!"
                });

                _context.Exams.Add(exam);

                try {
                    await _context.SaveChangesAsync();
                }
                catch {
                    return Conflict(new Response {
                        Status="Error",
                        Message=$"Some fields conflict with database, check again or contact us!"
                    });
                }

                return Ok(new ExamDTO(exam));
                
            }

            var errors = results.Select(item => item.ErrorMessage);

            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });

            
        }

        [HttpPost("PostForClass")]
        public async Task<IActionResult> PostForClassAsync(string className, string subjectCode, string roomCode, DateTime date, string timeString, string examForm)
        {

            var checkClass = await _context.Classes.FirstOrDefaultAsync(item => item.Name == className);
            if(checkClass == null) return NotFound(new Response {
                Status="Error",
                Message="Class not found!"
            });

            var checkSubject = await _context.Subjects.FirstOrDefaultAsync(item => item.Code == subjectCode);
            if(checkSubject == null) return NotFound(new Response {
                Status="Error",
                Message="Subject not found!"
            });

            var checkRoomCode = await _context.Rooms.FirstOrDefaultAsync(item => item.RoomCode == roomCode);
            if(checkRoomCode.IsActive == false) return BadRequest(new Response {
                Status="Error",
                Message="Room not active!"
            });

            var courses = await _context.RegisterCourses.Where(item => item.SubjectCode == subjectCode).ToListAsync();
            var filteredCourses = new List<RegisterCourse>();
            foreach (var course in courses)
            {
                StudentProfile student = await _context.StudentProfiles.Include(item => item.Classes).FirstOrDefaultAsync(item => item.RollNumber == course.StudentRollNumber);
                Semester semester = await _context.Semesters.FirstOrDefaultAsync(item => item.StartDate.Date <= date.Date && date.Date <= item.EndDate.Date);

                if(student.Classes.Contains(checkClass) && semester != null && course.isPayment) {
                    filteredCourses.Add(course);
                }

            }

            if(filteredCourses.Count == 0) return NotFound(new Response {
                Status="Error",
                Message="No have any course match with given data!"
            });

            var exams = new List<Exam>();

            foreach(var course in filteredCourses)
            {
                Exam exam = new Exam() {
                    RegisterCourseId=course.Id,
                    Date=date.Date,
                    RoomCode=roomCode,
                    Time=timeString,
                    ExamForm=examForm
                };

                _context.Exams.Add(exam);
                exams.Add(exam);
            }

            try {
                await _context.SaveChangesAsync();
            }
            catch {
                return StatusCode(StatusCodes.Status500InternalServerError, new Response{
                    Status="ServerError",
                    Message="An error while handle request!"
                });
            }

            return Ok(exams.Select(item => new ExamDTO(item)));
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            var items = await _context.Exams.Include(item => item.Course).ToListAsync();

            return Ok(items.Select(exam => new ExamDTO(exam)));
        }

        [HttpGet("StudentExam")]
        public async Task<IActionResult> StudentExamAsync(string StudentCode, string SemesterCode)
        {

            if(StudentCode==null) return BadRequest(new Response{
                Status="Error",
                Message="Missing StudentCode!"
            });

            var checkStudent = await _context.StudentProfiles.FirstOrDefaultAsync(item => item.RollNumber==StudentCode);
            if(checkStudent==null) return NotFound(new Response{
                Status="Error",
                Message=$"Student with RollNumber='{StudentCode}' not exist!"
            });

            if(SemesterCode!=null){

                var checkSemester = await _context.Semesters.FirstOrDefaultAsync(item => item.Code==SemesterCode);
                if(checkSemester==null) return NotFound(new Response{
                    Status="Error",
                    Message=$"Semester with Code='{SemesterCode}' not exist!"
                });
            }

            var courseIdList = await _context.RegisterCourses.Where(item => item.StudentRollNumber==StudentCode && (SemesterCode==null || item.SemesterCode==SemesterCode)).Select(item => item.Id).ToListAsync();

            var exams = await _context.Exams.Include(item => item.Course).Where(item => !item.isDone && courseIdList.Contains(item.RegisterCourseId)).ToListAsync();

            return Ok(exams.Select(exam => new ExamDTO(exam)));
        }

        [HttpPut("{Id}")]
        public async Task<IActionResult> UpdateAsync(int Id, ExamDTO dto)
        {
            Exam checkExam = await _context.Exams.FindAsync(Id);
            if(checkExam==null) return NotFound(new Response{
                Status="Error",
                Message=$"The exam with Id='{Id}' not exist!"
            });

            Exam exam = new Exam(dto);

            var context = new ValidationContext(exam);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(exam, context, results);

            if (isValid)
            {
                var checkCourse = await _context.RegisterCourses.FindAsync(exam.RegisterCourseId);
                if(checkCourse == null) return Conflict(new Response{
                    Status="Error",
                    Message=$"The course with Id='{exam.RegisterCourseId}' not exist!"
                });

                if(checkCourse.isPayment == false) return StatusCode(StatusCodes.Status406NotAcceptable, new {
                    Status="Waring",
                    Message=$"This course is unpaid!",
                    data=new RegisterCourseDTO(checkCourse)
                });
                
                var checkRoom = await _context.Rooms.FirstOrDefaultAsync(item => item.RoomCode==exam.RoomCode);
                if(checkRoom==null) return Conflict(new Response{
                    Status="Error",
                    Message=$"Room with Code='{exam.RoomCode}' not exist!"
                });

                if(checkRoom.IsActive == false) return StatusCode(StatusCodes.Status406NotAcceptable, new Response{
                    Status="Waring",
                    Message=$"The room {checkRoom.RoomCode} is not active!"
                });

                checkExam.Course = checkCourse;
                checkExam.Update(dto);
                _context.Exams.Update(checkExam);

                try {
                    await _context.SaveChangesAsync();
                }
                catch {
                    return Conflict(new Response {
                        Status="Error",
                        Message=$"Some fields conflict with database, check again or contact us!"
                    });
                }

                return Ok(new ExamDTO(checkExam));
                
            }

            var errors = results.Select(item => item.ErrorMessage);

            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });
        }
    
        [HttpPut("MarkToDone/{Id}")]
        public async Task<IActionResult> MarkToDoneAsync(int Id) {

            var checkExam = await _context.Exams.FindAsync(Id);
            if(checkExam == null) return NotFound(new Response {
                Status="Error",
                Message="Exam Not Found!"
            });

            checkExam.isDone = true;
            _context.Exams.Update(checkExam);
            try {

                await _context.SaveChangesAsync();
            }
            catch {
                return StatusCode(StatusCodes.Status500InternalServerError, new Response{
                    Status="ServerError",
                    Message="An error while handle request!"
                });
            }

            return Ok(new ExamDTO(checkExam));

        }

        [HttpDelete("{Id}")]
        public async Task<IActionResult> DeleteAsync(int Id)
        {
            Exam checkExam = await _context.Exams.Include(item => item.Course).FirstOrDefaultAsync(item => item.Id==Id);
            if(checkExam==null) return NotFound(new Response{
                Status="Error",
                Message=$"The exam with Id='{Id}' not exist!"
            });

            _context.Exams.Remove(checkExam);
            try{
                await _context.SaveChangesAsync();
            }
            catch {
                return Conflict(new Response {
                    Status="Error",
                    Message=$"Some fields conflict with database, check again or contact us!"
                });
            }

            return Ok(new {
                Status="Success",
                Message=$"Deleted Exam with Id='{Id}' success.",
                data=new ExamDTO(checkExam)
            });
        }
    }
}