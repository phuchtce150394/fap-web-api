using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FapWebApi.Data;
using FapWebApi.Models;

namespace FapWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MajorGroupController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public MajorGroupController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/MajorGroup
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MajorGroup>>> GetMajorGroups()
        {
            return await _context.MajorGroups.ToListAsync();
        }

        // GET: api/MajorGroup/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MajorGroup>> GetMajorGroup(int id)
        {
            var majorGroup = await _context.MajorGroups.FindAsync(id);

            if (majorGroup == null)
            {
                return NotFound();
            }

            return majorGroup;
        }

        // PUT: api/MajorGroup/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMajorGroup(int id, MajorGroup majorGroup)
        {
            if (id != majorGroup.Id)
            {
                return BadRequest();
            }

            _context.Entry(majorGroup).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MajorGroupExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MajorGroup
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<MajorGroup>> PostMajorGroup(MajorGroup majorGroup)
        {

            var checkCode = _context.MajorGroups.Any(item => item.Code == majorGroup.Code);
            if(checkCode) return Conflict(new {
                Status="Error",
                Message=$"Code '{majorGroup.Code}' already existed!"
            });

            _context.MajorGroups.Add(majorGroup);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (MajorGroupExists(majorGroup.Id))
                {
                    return Conflict(new {
                        Status="Error",
                        Message=$"Id should be put to zero!"
                    });
                }
                else
                {
                    return Conflict(new {
                        Status="Error",
                        Message=$"Some errors to add new instance!"
                    });;
                }
            }

            return CreatedAtAction("GetMajorGroup", new { id = majorGroup.Id }, majorGroup);
        }

        // DELETE: api/MajorGroup/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMajorGroup(int id)
        {
            var majorGroup = await _context.MajorGroups.FindAsync(id);
            if (majorGroup == null)
            {
                return NotFound();
            }

            _context.MajorGroups.Remove(majorGroup);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool MajorGroupExists(int id)
        {
            return _context.MajorGroups.Any(e => e.Id == id);
        }
    }
}
