using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FapWebApi.Data;
using Microsoft.AspNetCore.Mvc;
using FapWebApi.Models;
using FapWebApi.DTO;
using Microsoft.EntityFrameworkCore;
using FapWebApi.Tools;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace FapWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterCourseController : ControllerBase
    {
        ApplicationDbContext _context;

        public RegisterCourseController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpPost]
        public async Task<IActionResult> Post(RegisterCourseDTO dto)
        {
            RegisterCourse obj = new RegisterCourse(dto);

            var checkStudentRollNumber = await _context.StudentProfiles.AnyAsync(student => student.RollNumber == obj.StudentRollNumber);
            if(checkStudentRollNumber == false) return BadRequest(new Response{
                Status="Error",
                Message=$"StudentRollNumber '{obj.StudentRollNumber}' not existed!"
            });

            var checkSubjectCode = await _context.Subjects.AnyAsync(subject => subject.Code == obj.SubjectCode);
            if(checkSubjectCode == false) return BadRequest(new Response{
                Status="Error",
                Message=$"SubjectCode '{obj.SubjectCode}' not existed!"
            });

            var checkSemesterCode = await _context.Semesters.AnyAsync(semester => semester.Code == obj.SemesterCode);
            if(checkSemesterCode == false) return BadRequest(new Response{
                Status="Error",
                Message=$"SemesterCode '{obj.SemesterCode}' not existed!"
            });

            var context = new ValidationContext(obj);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(obj, context, results);

            if (isValid)
            {
                
                _context.RegisterCourses.Add(obj);

                try {
                    await _context.SaveChangesAsync();
                }
                catch {
                    return Conflict(new Response {
                        Status="Error",
                        Message=$"Some fields conflict with database, check again or contact us!"
                    });
                }

                return Ok(new RegisterCourseDTO(obj));
                
            }

            var errors = results.Select(item => item.ErrorMessage);

            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });
        }

        [HttpPost("RegisterByTermNo")]
        public async Task<IActionResult> RegisterByTermNoAsync(string StudentCode, string SemesterCode, int TermNo)
        {

            var checkStudent = await _context.StudentProfiles.FirstOrDefaultAsync(item => item.RollNumber == StudentCode);
            if(checkStudent == null) return NotFound(new Response{
                Status="Error",
                Message=$"Student with Code='{StudentCode}' not exist!"
            });

            var checkSemester = await _context.Semesters.FirstOrDefaultAsync(item => item.Code == SemesterCode);
            if(checkSemester == null) return NotFound(new Response{
                Status="Error",
                Message=$"Semester with Code='{SemesterCode}' not exist!"
            });

            if(TermNo < 0) return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message="TermNo must be non-negative!"
            });

            var subjects = await _context.Subjects.Where(item => item.MajorId == checkStudent.MajorId && item.TermNo==TermNo).ToListAsync();
            var courses = subjects.Select(subject => new RegisterCourse(){
                StudentRollNumber=StudentCode,
                SubjectCode=subject.Code,
                SemesterCode=SemesterCode
            });


            List<RegisterCourse> filterCourses = new List<RegisterCourse>();
            foreach(var course in courses)
            {
                var checkCourseExist = await _context.RegisterCourses.FirstOrDefaultAsync(item => item.StudentRollNumber==StudentCode && item.SubjectCode==course.SubjectCode && item.SemesterCode==SemesterCode);
                if(checkCourseExist==null) {
                    course.Status = "Enrolled";
                    _context.RegisterCourses.Add(course);
                    filterCourses.Add(course);
                }
                
            }
            
            try {
                await _context.SaveChangesAsync();
            }
            catch {
                return Conflict(new Response{
                    Status="Error",
                    Message="Some errors occur while handle request. Try again or contact us!"
                });
            }

            return Ok(filterCourses.Select(item => new RegisterCourseDTO(item)));
        }


        [HttpPost("RegisterCourseByClass")]
        public async Task<IActionResult> RegisterCourseByClassAsync(string ClassName, string SemesterCode, int TermNo)
        {
            var checkClass = await _context.Classes.Include(item => item.StudentProfiles).FirstOrDefaultAsync(item => item.Name == ClassName);
            if(checkClass == null) return NotFound(new Response{
                Status="Error",
                Message=$"ClassName not found!"
            });

            foreach (var student in checkClass.StudentProfiles)
            {
                await RegisterByTermNoAsync(student.RollNumber, SemesterCode, TermNo);
            }

            

            return Ok(new Response{
                Status="Success",
                Message="Done. Please check again..."
            });
        }

        [HttpGet]
        public async Task<IActionResult> GetAll(int pageIndex=1, int pageSize=2)
        {
            try {
                var courses = _context.RegisterCourses.OrderBy(item => item.Id);

                var paginatedItems = await PaginatedList<RegisterCourse>.CreateAsync(courses.AsNoTracking(), pageIndex, pageSize);

                if(paginatedItems.TotalPages > 0 && paginatedItems.PageIndex > paginatedItems.TotalPages) return NotFound();

                return Ok(paginatedItems.Select(item => new RegisterCourseDTO(item)));
            }
            catch {
                return StatusCode(500, new Response {
                    Status="Error",
                    Message="Server handle error, try again!"
                });
            }

        }

        [HttpGet("Filter")]
        public IActionResult Filter(string StudentRollNumber, string SubjectCode, string SemesterCode, string Status)
        {
            var filterItems = _context.RegisterCourses.Where(item => StudentRollNumber == null || item.StudentRollNumber == StudentRollNumber)
                            .Where(item => SubjectCode == null || item.SubjectCode == SubjectCode)
                            .Where(item => SemesterCode == null || item.SemesterCode == SemesterCode)
                            .Where(item => Status == null || item.Status == Status);

            return Ok(filterItems.Select(item => new RegisterCourseDTO(item)));

        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> GetAsync(int Id)
        {
            RegisterCourse obj = await _context.RegisterCourses.FindAsync(Id);

            if(obj == null) {
                return NotFound(new Response {
                    Status="Error",
                    Message=$"Not Found RegisterCourse of Id='{Id}'"
                });
            }

            return Ok(new RegisterCourseDTO(obj));
        }

        [HttpPut("{Id}")]
        public async Task<IActionResult> Put(int Id, RegisterCourseDTO dto)
        {
            RegisterCourse obj = await _context.RegisterCourses.FindAsync(Id);

            if(obj == null) {
                return NotFound(new Response {
                    Status="Error",
                    Message=$"Not Found RegisterCourse of Id='{Id}'"
                });
            }

            obj.StudentRollNumber=dto.StudentRollNumber;
            obj.SubjectCode=dto.SubjectCode;
            obj.SemesterCode=dto.SemesterCode;
            obj.Status=dto.Status;

            var checkStudentRollNumber = await _context.StudentProfiles.AnyAsync(student => student.RollNumber == obj.StudentRollNumber);
            if(checkStudentRollNumber == false) return BadRequest(new Response{
                Status="Error",
                Message=$"StudentRollNumber '{obj.StudentRollNumber}' not existed!"
            });

            var checkSubjectCode = await _context.Subjects.AnyAsync(subject => subject.Code == obj.SubjectCode);
            if(checkSubjectCode == false) return BadRequest(new Response{
                Status="Error",
                Message=$"SubjectCode '{obj.SubjectCode}' not existed!"
            });

            var checkSemesterCode = await _context.Semesters.AnyAsync(semester => semester.Code == obj.SemesterCode);
            if(checkSemesterCode == false) return BadRequest(new Response{
                Status="Error",
                Message=$"SemesterCode '{obj.SemesterCode}' not existed!"
            });

            var context = new ValidationContext(obj);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(obj, context, results);

            if (isValid)
            {
                
                _context.RegisterCourses.Update(obj);

                try {
                    await _context.SaveChangesAsync();
                }
                catch {
                    return Conflict(new Response {
                        Status="Error",
                        Message=$"Some fields conflict with database, check again or contact us!"
                    });
                }

                return Ok(new RegisterCourseDTO(obj));
                
            }

            var errors = results.Select(item => item.ErrorMessage);

            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });

        }

        [HttpDelete("{Id}")]
        public async Task<IActionResult> DeleteAsync(int Id)
        {
            RegisterCourse obj = await _context.RegisterCourses.FindAsync(Id);

            if(obj == null) {
                return NotFound(new Response {
                    Status="Error",
                    Message=$"Not Found RegisterCourse of Id='{Id}'"
                });
            }

            if(obj.isPayment) return BadRequest(new Response{
                Status="Error",
                Message=$"This course is paid. Can't delete this object!"
            });

            try {
                _context.RegisterCourses.Remove(obj);
                await _context.SaveChangesAsync();

                return Ok(new Response {
                    Status="Success",
                    Message=$"RegisterCourse of '{obj.StudentRollNumber}' with Subject '{obj.SubjectCode}' at Semester '{obj.SemesterCode}' has been deleted."
                });
            }
            catch {
                return Conflict(new Response {
                    Status="Error",
                    Message="An error handle while processing data, try again or contact us!"
                });
            }

        }

    }
}