using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FapWebApi.Data;
using Microsoft.AspNetCore.Mvc;
using FapWebApi.Models;
using FapWebApi.Identity;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using FapWebApi.DTO;
using FapWebApi.Tools;

namespace FapWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminProfileController : ControllerBase
    {

        ApplicationDbContext _context;

        public AdminProfileController(ApplicationDbContext context) {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync(int pageIndex=1, int pageSize=2) 
        {
            try {
                var admins = _context.AdminProfiles.OrderBy(item => item.Name);

                var paginatedAdmins = await PaginatedList<AdminProfile>.CreateAsync(admins.AsNoTracking(), pageIndex, pageSize);

                if(paginatedAdmins.TotalPages > 0 && paginatedAdmins.PageIndex > paginatedAdmins.TotalPages) return NotFound();

                return Ok(paginatedAdmins.Select(profile => new AdminProfileDTO(profile)));
            }
            catch {
                return StatusCode(500, new Response {
                    Status="Error",
                    Message="Server handle error, try again!"
                });
            }
        }

        [HttpGet("{Id}")]
        public async Task<IActionResult> Get(int Id)
        {
            AdminProfile profile = await _context.AdminProfiles.FindAsync(Id);

            if(profile == null) {
                return NotFound(new Response {Status="Error", Message=$"No have any AdminProfile with Id '{Id}'"});
            }

            AdminProfileDTO dto = new AdminProfileDTO(profile);

            return Ok(dto);

        }

        [HttpGet("GetByUserId")]
        public async Task<IActionResult> Get(string userId) 
        {
            AdminProfile profile = await _context.AdminProfiles.FirstOrDefaultAsync(item => item.AccountId == userId);

            if(profile == null) {
                return NotFound(new Response {Status="Error", Message=$"No have any AdminProfile with userId='{userId}'"});
            }

            AdminProfileDTO dto = new AdminProfileDTO(profile);

            return Ok(dto);

        }

        [HttpPut]
        public async Task<IActionResult> Update(AdminProfileDTO dto)
        {
            AdminProfile adminProfile = await _context.AdminProfiles.FindAsync(dto.Id);

            if(adminProfile == null) {
                return NotFound(new Response {Status="Error", Message=$"No have any Admin with Id '{dto.Id}'"});
            }

            if(dto.Name != null) adminProfile.Name=dto.Name;
            if(dto.ImageURL != null) adminProfile.ImageURL=dto.ImageURL;
            if(dto.Gender != null) adminProfile.Gender=dto.Gender.Value;
            if(dto.RollNumber != null) adminProfile.RollNumber=dto.RollNumber;
            if(dto.Birthday != null) adminProfile.Birthday=dto.Birthday;
            if(dto.UserId != null) adminProfile.AccountId=dto.UserId;

            var context = new ValidationContext(adminProfile);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(adminProfile, context, results);

            if (isValid)
            {
                _context.AdminProfiles.Update(adminProfile);
                try {

                    await _context.SaveChangesAsync();
                    return Ok(new AdminProfileDTO(adminProfile));
                }
                catch {
                    return Conflict(new Response{
                        Status="Error",
                        Message="An error while handle data. Try again or contact us!"
                    });
                }
            }

            var errors = results.Select(item => item.ErrorMessage);
            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });

        }

        [HttpDelete("{Id}")]
        public async Task<IActionResult> Delete(int Id) {

            AdminProfile adminProfile = await _context.AdminProfiles.FindAsync(Id);

            if(adminProfile == null) {
                return NotFound(new Response {
                    Status="Error",
                    Message=$"AdminProfile with id='{Id}' not exist."
                });
            }

            _context.AdminProfiles.Remove(adminProfile);

            try {
                await _context.SaveChangesAsync();
            }
            catch {
                return Conflict(new Response {
                    Status="Error",
                    Message="Cann't delete this profile. Check all relationship with other data."
                });
            }

            return Ok(new Response {
                Status="Success",
                Message=$"AdminProfile of '{adminProfile.Name}' deleted."
            });

        }

    }



}
