using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using FapWebApi.Data;
using FapWebApi.DTO;
using FapWebApi.Tools;
using FapWebApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;

namespace FapWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class SubjectController : ControllerBase{
        ApplicationDbContext _context;
        public SubjectController(ApplicationDbContext context)
        {
            _context = context;
        }
        
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {

            var subjects = await _context.Subjects.Include(item => item.Major).OrderBy(item => item.MajorId).ThenBy(item => item.TermNo).Select(item => new SubjectDTO(item)).ToListAsync();

            return Ok(subjects);
        }

     
        [HttpGet("{Id}")]
        public async Task<IActionResult> GetAsync(int Id)
        {
            Subject subject = await _context.Subjects.Include(item => item.Major).FirstOrDefaultAsync(item => item.Id == Id);
            if(subject == null) return NotFound(new Response{
                Status="Error",
                Message=$"Subject with Id='{Id}' not found!"
            });

            return Ok(new SubjectDTO(subject));
        }

        [HttpGet("GetByCode")]
        public async Task<IActionResult> GetByCodeAsync(string subjectCode)
        {
            Subject subject = await _context.Subjects.Include(item => item.Major).FirstOrDefaultAsync(item => item.Code == subjectCode);
            if(subject == null) return NotFound(new Response{
                Status="Error",
                Message=$"Subject with Code='{subjectCode}' not found!"
            });

            return Ok(new SubjectDTO(subject));
        }


        [HttpPut("{Id}")]
        public async Task<IActionResult> Put(int Id, SubjectDTO dto)
        {

            Subject subject = await _context.Subjects.FindAsync(Id);

            if(subject == null) return NotFound(new Response {
                Status="Error",
                Message=$"Subject with Id '{Id}' not found!"
            });

            var checkCodeDupdicate = await _context.Subjects.AnyAsync(item => item.Code.Equals(dto.Code) && !item.Code.Equals(subject.Code));
            if(checkCodeDupdicate) return Conflict(new Response {
                Status="Error",
                Message=$"Code '{dto.Code}' already exist!"
            });

            subject.Code=dto.Code;
            subject.Type=dto.Type;
            subject.Name=dto.Name;
            subject.TermNo=dto.TermNo;
            subject.Credit=dto.Credit;
            subject.Fee=dto.Fee;

            if(dto.MajorCode != null) {
                
                Major major = await _context.Majors.FirstOrDefaultAsync(item => item.Code == dto.MajorCode);
                if(major == null) return Conflict(new Response{
                    Status="Error",
                    Message=$"MajorCode '{dto.MajorCode}' not exist!"
                });

                subject.MajorId = major.Id;
                subject.Major   = major;
                
            }

            var context = new ValidationContext(subject);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(subject, context, results);

            if (isValid)
            {
                
                _context.Subjects.Update(subject);

                try {
                    await _context.SaveChangesAsync();
                }
                catch {
                    return Conflict(new Response {
                        Status="Error",
                        Message=$"Some fields conflict with database, check again or contact us!"
                    });
                }

                return Ok(new SubjectDTO(subject));
                
            }

            var errors = results.Select(item => item.ErrorMessage);

            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });

        }


        [HttpPost]
        public async Task<IActionResult> Post(SubjectDTO dto)
        {


            Subject subject = new Subject(dto);

            if(dto.MajorCode != null) {
                Major major = await _context.Majors.FirstOrDefaultAsync(item => item.Code == dto.MajorCode);
                if(major == null) return Conflict(new Response{
                    Status="Error",
                    Message=$"MajorCode '{dto.MajorCode}' not exist!"
                });

                subject.MajorId = major.Id;
                subject.Major   = major;
                
            }


            var checkCodeDupdicate = await _context.Subjects.AnyAsync(item => item.Code == subject.Code);
            if(checkCodeDupdicate) return Conflict(new Response {
                Status="Error",
                Message=$"Code '{subject.Code}' duplicated!"
            });

            var context = new ValidationContext(subject);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(subject, context, results);

            if (isValid)
            {
                
                _context.Subjects.Add(subject);

                try {
                    await _context.SaveChangesAsync();
                }
                catch {
                    return Conflict(new Response {
                        Status="Error",
                        Message=$"Some fields conflict with database, check again or contact us!"
                    });
                }

                return Ok(new SubjectDTO(subject));
                
            }

            var errors = results.Select(item => item.ErrorMessage);

            return StatusCode(StatusCodes.Status406NotAcceptable, new Response {
                Status="Error",
                Message=errors.FirstOrDefault()
            });
        }


        [HttpDelete("{Id}")]
        public async Task<IActionResult> Delete(int Id)
        {
            Subject subject = await _context.Subjects.FindAsync(Id);

            if(subject == null) return NotFound(new Response {
                Status="Error",
                Message=$"Subject with Id '{Id}' not found!"
            });

            try {

                _context.Subjects.Remove(subject);
                await _context.SaveChangesAsync();
                return Ok(new Response {
                    Status="Success",
                    Message=$"Subject '{subject.Code}' has been deleted."
                });
            }
            catch {
                return Conflict(new Response {
                    Status="Error",
                    Message="An error handle while processing data, try again or contact us!"
                });
            }

            
        }

    }
}