using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FapWebApi.Models;
using FapWebApi.Data;
using FapWebApi.DTO;
using Microsoft.EntityFrameworkCore;
using FapWebApi.Tools;

namespace FapWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentFeeController : ControllerBase
    {

        ApplicationDbContext _context;
        public StudentFeeController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet("SearchStudentFee")]
        public async Task<IActionResult> SearchStudentFeeAsync(string studentRollNumber, string semesterCode, bool? isComplete)
        {

            if(studentRollNumber == null) return BadRequest(new Response{
                Status="Error",
                Message="Missing studentRollNumber!"
            });

            var student = await _context.StudentProfiles.FirstOrDefaultAsync(profile => profile.RollNumber == studentRollNumber);

            if(student == null) {
                return NotFound(new Response {
                    Status="Error",
                    Message=$"Not Found StudentProfile of rollNumber='{studentRollNumber}'"
                });
            }

            if(semesterCode != null) {

                var semester = await _context.Semesters.FirstOrDefaultAsync(item => item.Code == semesterCode);

                if (semester == null) return NotFound(new Response{
                    Status="Erorr",
                    Message=$"Semester with Code='{semesterCode}' not found!"
                });
            }

            var coursesOfStudent = _context.RegisterCourses
                                .Where(item => item.StudentRollNumber == studentRollNumber)
                                .Where(item => semesterCode == null || item.SemesterCode == semesterCode)
                                .AsEnumerable().GroupBy(item => item.SemesterCode).ToList();

            List<StudentFeeDTO> studentFeeDTOs = new List<StudentFeeDTO>();
            foreach(var group in coursesOfStudent)
            {
                StudentFee current = new StudentFee(){
                    StudentRollNumber=studentRollNumber,
                    StudentName=student.Name,
                    SemesterCode=group.Key,
                    TotalFee=0,
                    Note="Include "
                };
               
                foreach(RegisterCourse course in group) {
                    Subject subject = await _context.Subjects.FirstOrDefaultAsync(item => item.Code == course.SubjectCode);
                    current.TotalFee += subject.Fee;
                    current.Note += subject.Code + " | ";
                }
                
                StudentFee oldFee = await _context.StudentFees.FirstOrDefaultAsync(item => item.StudentRollNumber==studentRollNumber && item.SemesterCode==group.Key);
                if(oldFee == null) {

                    try {
                        current.PaidFee = 0;
                        current.isComplete = false;
                        _context.StudentFees.Add(current);
                        await _context.SaveChangesAsync();
                    }
                    catch {
                        return StatusCode(500, new Response{
                            Status="Error",
                            Message="Server handle error, try again or contact us!"
                        });
                    }

                    studentFeeDTOs.Add(new StudentFeeDTO(current));
                }
                else {
                    if(oldFee.isComplete == false) {

                        if(oldFee.TotalFee != current.TotalFee || oldFee.Note != current.Note)
                        {
                            oldFee.TotalFee = current.TotalFee;
                            oldFee.Note = current.Note;
                            try {
                                _context.StudentFees.Update(oldFee);
                                await _context.SaveChangesAsync();
                            }
                            catch {
                                return StatusCode(500, new Response{
                                    Status="Error",
                                    Message="Server handle error, try again or contact us!"
                                });
                            }
                        }
                        studentFeeDTOs.Add(new StudentFeeDTO(oldFee));
                    }
                    else {
                        if(oldFee.TotalFee < current.TotalFee) {
                            oldFee.TotalFee = current.TotalFee;
                            oldFee.Note = current.Note;
                            oldFee.isComplete = false;

                            try {
                                _context.StudentFees.Update(oldFee);
                                await _context.SaveChangesAsync();
                            }
                            catch {
                                return StatusCode(500, new Response{
                                    Status="Error",
                                    Message="Server handle error, try again or contact us!"
                                });
                            }
                            
                        }

                        studentFeeDTOs.Add(new StudentFeeDTO(oldFee));
                        
                    }

                }
            
            }
            
            return Ok(studentFeeDTOs.Where(item => isComplete == null || item.isComplete==isComplete));
        }

        [HttpPost("ConfirmStudentPayment")]
        public async Task<IActionResult> ConfirmStudentPaymentAsync(StudentFeeDTO dto)
        {
            if(dto.StudentRollNumber == null) return BadRequest(new Response{
                Status="Error",
                Message="Missing studentRollNumber!"
            });

            var student = await _context.StudentProfiles.FirstOrDefaultAsync(profile => profile.RollNumber == dto.StudentRollNumber);

            if(student == null) {
                return NotFound(new Response {
                    Status="Error",
                    Message=$"Not Found StudentProfile of rollNumber='{dto.StudentRollNumber}'"
                });
            }

            if(dto.SemesterCode != null) {

                var semester = await _context.Semesters.FirstOrDefaultAsync(item => item.Code == dto.SemesterCode);

                if (semester == null) return NotFound(new Response{
                    Status="Erorr",
                    Message=$"Semester with Code='{dto.SemesterCode}' not found!"
                });
            } else {
                return BadRequest(new Response{
                    Status="Error",
                    Message="Missing SemesterCode!"
                });
            }

            var StudentFee = await _context.StudentFees.FirstOrDefaultAsync(item => item.StudentRollNumber==student.RollNumber && item.SemesterCode==dto.SemesterCode);

            if(StudentFee==null) return NotFound(new Response{
                Status="Error",
                Message=$"Fee of '{student.RollNumber}' in '{dto.SemesterCode}' not found! You can be research, and try again!"
            });


            var courses = await _context.RegisterCourses.Where(item => item.StudentRollNumber==StudentFee.StudentRollNumber && item.SemesterCode==StudentFee.SemesterCode).ToListAsync();
            var tempFee = 0;
            foreach(RegisterCourse course in courses) {
                Subject subject = await _context.Subjects.FirstOrDefaultAsync(item => item.Code == course.SubjectCode);
                tempFee += subject.Fee;
            }

            if(tempFee != StudentFee.TotalFee) return Conflict(new Response {
                Status="Error",
                Message=$"Current StudentFee of '{StudentFee.StudentRollNumber}' in system invalid. Please update before!"
            });

            
            if(StudentFee.isComplete) {
                return Conflict(new {
                    Status="Warning",
                    Message=$"Fee of student '{StudentFee.StudentRollNumber}' in '{StudentFee.SemesterCode}' is paid.",
                    data=new StudentFeeDTO(StudentFee)
                });
            }

            StudentFee.PaidFee = StudentFee.TotalFee;

            foreach(RegisterCourse course in courses) {
                
                course.isPayment = true;
                _context.RegisterCourses.Update(course);
            }

            StudentFee.isComplete = true;
            StudentFee.Note += $" complete at {DateTime.Today.ToShortDateString()}, {DateTime.Now.ToShortTimeString()} | " + dto.Note;
            _context.StudentFees.Update(StudentFee);

            try {
                await _context.SaveChangesAsync();
            }
            catch {
                return Conflict(new Response{
                    Status="Error",
                    Message="An error while handle request, try again or contact us!"
                });
            }

            return Ok(new StudentFeeDTO(StudentFee));
        }
        
        [HttpPut("UpdateStudentFee")]
        public async Task<IActionResult> UpdateAsync(string studentRollNumber) {

            return await SearchStudentFeeAsync(studentRollNumber, null, null);
        }
    }
}